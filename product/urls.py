from rest_framework import routers
from django.urls import path
from .api import ProductViewSet, CreatePostAPI, AddWihsListAPI, ProductViewSetLogged, UserWishListAPI, GetDescriptionAPI, UserPostsAPI

router = routers.DefaultRouter(trailing_slash=False)
router.register('api/products', ProductViewSet, 'product')
router.register('api/products-logged', ProductViewSetLogged, 'product_logged')
router.register('api/products-user',  UserPostsAPI, 'manage user created post')

urlpatterns = [
    path('api/product/create', CreatePostAPI.as_view(), name='create post'),
    path('api/product/get_description',
         GetDescriptionAPI.as_view(), name='get description'),
    path('api/product/user-wishlist',
         UserWishListAPI.as_view(), name='user wishlist'),
    path('api/product/wishlist',
         AddWihsListAPI.as_view(), name='add or remove from WishList'),
]

urlpatterns += router.urls
