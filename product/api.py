from rest_framework import viewsets, permissions, status, generics, filters
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from .serializers import ProductViewSerializer, ProductSerializer, ProductLoggedSerializer, ProductCreateSerializer
from .models import Product
import requests
from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
from requests.compat import quote_plus


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    permission_classes = [
        permissions.AllowAny,
    ]
    serializer_class = ProductViewSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_fields = ['category', ]
    search_fields = ['name', ]
    http_method_names = ['get']


class ProductViewSetLogged(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = ProductLoggedSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_fields = ['category', 'creator_id']
    search_fields = ['name', ]
    http_method_names = ['get']


class CreatePostAPI(APIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    parser_classes = [MultiPartParser, FormParser]

    def post(self, request, format=None):
        serializer = ProductCreateSerializer(
            data=request.data)
        if serializer.is_valid():
            serializer.save(creator=request.user)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserPostsAPI(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = ProductSerializer
    http_method_names = ['get', 'delete', 'patch']

    def retrieve(self, request, *args, **kwargs):
        user = self.request.user
        pk = self.kwargs.get('pk')
        data = ProductSerializer(
            Product.objects.get(creator=user.pk, id=pk)).data
        return Response(data, status=status.HTTP_200_OK)

    def get_queryset(self):
        user = self.request.user
        return Product.objects.filter(creator=user.pk)


class UserWishListAPI(generics.ListAPIView):
    serializer_class = ProductLoggedSerializer
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def get_queryset(self):
        user = self.request.user
        return Product.objects.filter(users_wishlist=user.pk)


# Add remove from WihsList


class AddWihsListAPI(APIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def post(self, request):
        product = get_object_or_404(Product, id=request.data.get('id'))
        if request.user not in product.users_wishlist.all():
            product.users_wishlist.add(request.user)
            return Response({'detail': 'User added to wishlist'}, status=status.HTTP_200_OK)
        else:
            product.users_wishlist.remove(request.user)
            return Response({'detail': 'User removed from wishlist'}, status=status.HTTP_200_OK)
        # return Response({'detail': 'An error has occurred'}, status=status.HTTP_400_BAD_REQUEST)


# Web scraping
BASE_BOOKS_URL = 'https://www.knygos.lt/lt/paieska?q={}'
SECOND_BOOKS_URL = 'https://www.knygos.lt/'
BASE_GAMES_URL = 'https://stalozaidimai.eu/?s={}&product_cat=&post_type=product&v=c562607189d7'


class GetDescriptionAPI(APIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def post(self, request):
        search = request.data['name']
        # nr = int(request.data['number']) - 1
        category = request.data['category']
        if category == 'Knygos':
            first_url = BASE_BOOKS_URL.format(quote_plus(search))
            response = requests.get(first_url)
            response_data = response.text
            soup = BeautifulSoup(response_data, features='html.parser')
            book_info = soup.find_all('div', {'class': 'book-properties'})
            if book_info:
                second_url = book_info[0].find('a').get('href')
                if second_url.startswith(SECOND_BOOKS_URL):
                    final_url = second_url
                else:
                    second_url = SECOND_BOOKS_URL + second_url
                    response = requests.get(second_url)
                    response_data = response.text
                    soup = BeautifulSoup(response_data, features='html.parser')
                    book_info = soup.find_all(
                        'div', {'class': 'product'})
                    final_url = book_info[0].find('a').get('href')
                response = requests.get(final_url)
                response_data = response.text
                soup = BeautifulSoup(response_data, features='html.parser')

                book_info = soup.find_all(
                    'div', {'class': 'book-descript-wrapper'})
                title_all = book_info[0].find(class_='book-title')
                title_all = title_all.find_all('span', {'itemprop': 'name'})
                title = title_all[0].text
                description = book_info[0].find(
                    class_='product-descript more-content-wrapper').text
                description = description.split("\nAprašymas\n", maxsplit=1)[1]
                description = description.strip()
                # description = description.replace('\r', '')
                if len(title_all) > 1:
                    author = title_all[1].text
                    full_description = 'Knyga: ' + title + '\nAutorius: ' + \
                        author + '\nAprašymas: \n' + description
                else:
                    full_description = 'Knyga: ' + title + '\nAprašymas: \n' + description
                return Response({'description': full_description}, status=status.HTTP_200_OK)
            else:
                return Response({'detail': 'Item not found'}, status=status.HTTP_200_OK)

        elif category == 'Stalo žaidimai':
            first_url = BASE_GAMES_URL.format(quote_plus(search))
            req = Request(first_url, headers={'User-Agent': 'Mozilla/5.0'})
            response_data = urlopen(req).read()
            soup = BeautifulSoup(response_data, features='html.parser')
            product_info = soup.find_all('h3', {'class': 'product-title'})
            if product_info:
                second_url = product_info[0].find('a').get('href')
                req = Request(second_url, headers={
                              'User-Agent': 'Mozilla/5.0'})
                response_data = urlopen(req).read()
                soup = BeautifulSoup(response_data, features='html.parser')
                product_title = soup.find('h1', {
                    'class': 'product_title entry-title'})
                title = product_title.text
                product_description = soup.find('div', {
                    'class': 'woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab'})
                description = product_description.get_text(separator="\n")
                full_description = 'Pavadinimas: ' + title + '\nAprašymas: \n' + description
                return Response({'description': full_description}, status=status.HTTP_200_OK)
            else:
                return Response({'detail': 'Item not found'}, status=status.HTTP_200_OK)
        else:
            return Response({'detail': 'Can not get description for this category'}, status=status.HTTP_400_BAD_REQUEST)
