from django.contrib import admin
from .models import Product
from django.utils.html import format_html


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'creator', 'description', 'creation_date',
                    'primary_value', 'location', 'for_sale', 'price', 'image_tag')
    search_fields = ('name', 'creator')
    readonly_fields = ('image_preview', 'id', 'creation_date')

    filter_horizontal = ('users_wishlist',)
    list_filter = ('for_sale',)
    fieldsets = ()

    def image_preview(self, obj):
        return format_html('<img src="{}" width="300" height="300" />'.format(obj.image.url))

    image_preview.short_description = 'Image preview'
    image_preview.allow_tags = True


admin.site.register(Product, ProductAdmin)
