from rest_framework import serializers
from .models import Product, Account


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('id', 'username', 'image')


class WishListAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('id',)


class ProductViewSerializer(serializers.ModelSerializer):
    creator = ProfileSerializer(many=False, read_only=True)

    class Meta:
        model = Product
        fields = ('id', 'category', 'name', 'image', 'description', 'creation_date',
                  'state', 'primary_value', 'location', 'for_sale', 'price', 'creator')


class ProductCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'creator', 'category', 'name', 'image', 'description',
                  'state', 'primary_value', 'location', 'for_sale', 'price')


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'creator', 'category', 'name', 'image', 'description',
                  'state', 'primary_value', 'location', 'for_sale', 'price')

    def save(self, *args, **kwargs):
        if self.validated_data.get('image', None) != None:
            if self.instance.image:
                self.instance.image.delete()
        return super().save(*args, **kwargs)


class ProductLoggedSerializer(serializers.ModelSerializer):
    creator = ProfileSerializer(many=False, read_only=True)
    # users_wishlist = WishListAccountSerializer(many=True, read_only=True)
    in_wishlist = serializers.SerializerMethodField('check_wishlist')

    class Meta:
        model = Product
        fields = ('id', 'category', 'name', 'image', 'description', 'creation_date',
                  'state', 'primary_value', 'location', 'for_sale', 'price', 'creator', 'in_wishlist')

    def check_wishlist(self, obj):
        if self.context['request'].user in obj.users_wishlist.all():
            in_wishlist = True
        else:
            in_wishlist = False
        return in_wishlist
