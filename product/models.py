import os
from django.db import models
from accounts.models import Account
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _


def upload_to(instance, filename):
    now = timezone.now()
    base, extension = os.path.splitext(filename.lower())
    return f"product_photo/{'product_'}{instance.name}{'_creator_'}{instance.creator}{'-ID'}{instance.creator.pk}{'_'}{now:%Y%m%d%H%M%S}{extension}"


class Product(models.Model):
    id = models.AutoField(primary_key=True)
    creator = models.ForeignKey(Account, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    image = models.ImageField(_("image"), upload_to=upload_to)
    description = models.TextField(blank=False)
    creation_date = models.DateTimeField(auto_now_add=True)
    primary_value = models.FloatField(null=True, default=None)
    location = models.CharField(max_length=100)
    for_sale = models.BooleanField(default=False)
    price = models.FloatField(null=True, default=None)
    users_wishlist = models.ManyToManyField(
        Account, related_name="user_wishlist", blank=True)

    categories = [
        ('Knygos', 'Knygos'),
        ('Stalo žaidimai', 'Stalo žaidimai'),
        ('Dviračiai / paspirtukai', 'Dviračiai / paspirtukai'),
        ('Kitos prekės', 'Kitos prekės'),
    ]

    category = models.CharField(
        choices=categories,
        max_length=50,
        default='Others'
    )

    states = [
        ('Nauja', 'Nauja'),
        ('Labai gera', 'Labai gera'),
        ('Gera', 'Gera'),
        ('Patenkinama', 'Patenkinama'),
    ]

    state = models.CharField(
        choices=states,
        max_length=50,
        default='Others'
    )

    def delete(self):
        self.image.storage.delete(self.image.name)
        super().delete()

    def image_tag(self):
        return mark_safe('<img src="/../../media/%s" width="30" height="30" />' % (self.image))

    image_tag.short_description = 'Image'

    class Meta:
        ordering = ('-creation_date',)

    def __str__(self):
        return self.name
