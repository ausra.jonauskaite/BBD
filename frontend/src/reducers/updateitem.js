import { LOAD_ITEM, UPDATE_ITEM, DELETE_ITEM } from '../actions/types.js';

const initialState = {
    product: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case LOAD_ITEM:
            return {
                ...state,
                product: action.payload
            };
        case UPDATE_ITEM:
            return {
                ...state,
                product: action.payload
            };
        case DELETE_ITEM:
            return {
                ...state,
                product: state.product.id
            };
        default:
            return state;
    }
}