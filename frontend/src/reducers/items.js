import { GET_ITEMS, FETCH_BOOKS, FETCH_GAMES, FETCH_BIKES, FETCH_OTHER } from '../actions/types.js';

const initialState = {
    items: [],
    loading: false,
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_ITEMS:
            return {
                ...state,
                loading: true,
                items: action.payload
            };
        case FETCH_BOOKS:
            return {
                ...state,
                loading: true,
                items: action.payload
            };
        case FETCH_GAMES:
            return {
                ...state,
                loading: true,
                items: action.payload,
            };
        case FETCH_BIKES:
            return {
                ...state,
                loading: true,
                items: action.payload,
            };
        case FETCH_OTHER:
            return {
                ...state,
                loading: true,
                items: action.payload,
            };
        default:
            return state;
    }
}