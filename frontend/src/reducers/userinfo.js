import { GET_USERINFO, GET_USERITEMS } from '../actions/types.js';

const initialState = {
    user: [],
    items: [],
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_USERINFO:
            return {
                ...state,
                user: action.payload
            };
        case GET_USERITEMS:
            return {
                ...state,
                items: action.payload
            };
        default:
            return state;
    }
}