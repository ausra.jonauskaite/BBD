import { SENT_SUCCESS } from '../actions/types.js';

const initialState = {
    sent: {},
    successStatus: ''
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SENT_SUCCESS:
            return {
                ...state,
                sent: action.payload,
                successStatus: action.successStatus
            };
        default:
            return state;
    }
}