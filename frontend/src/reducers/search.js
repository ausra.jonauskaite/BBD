import { GET_PRIVATESEARCH_TERM } from '../actions/types.js';

const initialState = {
    privateSearchResults: [],
    privatename: ''
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_PRIVATESEARCH_TERM:
            return {
                ...state,
                privateSearchResults: action.payload,
                privatename: action.privatename
            };
        default:
            return state;
    }
}