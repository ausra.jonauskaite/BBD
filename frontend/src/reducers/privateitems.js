import { GET_PRIVATEITEMS } from '../actions/types.js';

const initialState = {
    items: []
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_PRIVATEITEMS:
            return {
                ...state,
                items: action.payload
            };
        default:
            return state;
    }
}