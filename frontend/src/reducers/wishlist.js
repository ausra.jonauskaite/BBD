import { GET_WISHLIST, POST_WISHLIST } from '../actions/types.js';

const initialState = {
    items: []
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_WISHLIST:
            return {
                ...state,
                items: action.payload
            };
        case POST_WISHLIST:
            return {
                ...state
            };
        default:
            return state;
    }
}