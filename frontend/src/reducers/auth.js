import { USER_LOADED, USER_LOADING, AUTH_ERROR, LOGIN_FAIL, LOGIN_SUCCESS, LOGOUT_SUCCESS, REGISTER_SUCCESS, REGISTER_FAIL, USERUPDATE_FAIL, USERUPDATE_SUCCESS, PSWUPDATE_SUCCESS, PSWUPDATE_FAIL, ALL_READ_SUCCESS, GET_MESSAGES, ONE_READ_SUCCESS } from '../actions/types';

const initialState = {
    token: localStorage.getItem('token'),
    isAuthenticated: null,
    isLoading: false,
    user: null,
    notifications: [],
    status: ''
}

export default function (state = initialState, action) {
    switch (action.type) {
        case USER_LOADING:
        case PSWUPDATE_FAIL:
            return {
                ...state,
                isLoading: true
            };
        case USER_LOADED:
        case USERUPDATE_SUCCESS:
            return {
                ...state,
                isAuthenticated: true,
                isLoading: false,
                user: action.payload
            };
        case PSWUPDATE_SUCCESS:
        case USERUPDATE_FAIL:
            return {
                ...state,
                isAuthenticated: true,
                isLoading: false
            };
        case LOGIN_SUCCESS:
        case REGISTER_SUCCESS:
            localStorage.setItem('token', action.payload.token);
            return {
                ...state,
                ...action.payload,
                isAuthenticated: true,
                isLoading: false
            }
        case AUTH_ERROR:
        case LOGIN_FAIL:
        case LOGOUT_SUCCESS:
        case REGISTER_FAIL:
            localStorage.removeItem('token');
            return {
                ...state,
                token: null,
                user: null,
                isAuthenticated: false,
                isLoading: false
            };
        case ALL_READ_SUCCESS:
        case ONE_READ_SUCCESS:
            return {
                ...state,
                status: action.payload
            };
        case GET_MESSAGES:
            return {
                ...state,
                notifications: action.payload
            };
        default:
            return state;
    }
}