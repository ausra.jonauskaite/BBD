import { GET_PRODUCT, GET_PRIVATEPRODUCT } from '../actions/types.js';

const initialState = {
    product: {
        creator: {}
    },
    privateproduct: {
        creator: {}
    },
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_PRODUCT:
            return {
                ...state,
                product: action.payload
            };
        case GET_PRIVATEPRODUCT:
            return {
                ...state,
                privateproduct: action.payload
            };
        default:
            return state;
    }
}