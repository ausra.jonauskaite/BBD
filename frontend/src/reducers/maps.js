import { GET_COORDINATES, GET_RADIUS, GET_RESULTS } from '../actions/types.js';

const initialState = {
    coordinates: [],
    productsList: [],
    filterRadius: ''

}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_COORDINATES:
            return {
                ...state,
                coordinates: action.coordinates
            };
        case GET_RESULTS:
            return {
                ...state,
                productsList: action.productsList
            };
        case GET_RADIUS:
            return {
                ...state,
                filterRadius: action.filterRadius
            };
        default:
            return state;
    }
}