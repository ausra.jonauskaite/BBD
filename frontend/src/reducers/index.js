import { combineReducers } from 'redux';
import auth from './auth';
import userinfo from './userinfo';
import items from './items';
import product from './product';
import privateitems from './privateitems';
import updateitem from './updateitem';
import send from './send';
import wishlist from './wishlist';
import errors from './errors';
import search from './search';
import maps from './maps';


export default combineReducers({
    auth,
    userinfo,
    items,
    product,
    privateitems,
    updateitem,
    send,
    wishlist,
    errors,
    search,
    maps
});
