import axios from 'axios';
import { tokenConfig } from './auth';
import { SENT_SUCCESS, GET_ERRORS } from './types';

// send form info
export const sendForm = (get_item, give_item, message, show_email, show_phone) => (dispatch, getState) => {

    const body = JSON.stringify({ get_item, give_item, message, show_email, show_phone });
    // console.log(body);
    axios
        .post('/api/auth/send', body, tokenConfig(getState))
        .then(res => {
            dispatch({
                type: SENT_SUCCESS,
                payload: res.data,
                successStatus: res.status
            });
        }).catch(err => {
            const errors = {
                msg: err.response.data,
                status: err.response.status
            }
            // dispatch(returnErrors(err.response.data, err.response.status));
            dispatch({
                // type: LOGIN_FAIL
                type: GET_ERRORS,
                payload: errors
            });
        });
};