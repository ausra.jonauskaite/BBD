import { GET_COORDINATES, GET_RESULTS, GET_RADIUS } from './types';


// GET USER COORDINATES
export const getUserData = (userCoordinates) => (dispatch) => {

    dispatch({
        type: GET_COORDINATES,
        coordinates: userCoordinates
    });

};

// GET USER COORDINATES
export const getResultsData = (results) => (dispatch) => {

    dispatch({
        type: GET_RESULTS,
        productsList: results
    });

};

// GET USER COORDINATES
export const getRadiusData = (radius) => (dispatch) => {

    dispatch({
        type: GET_RADIUS,
        filterRadius: radius
    });

};