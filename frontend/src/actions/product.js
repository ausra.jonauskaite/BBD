import axios from 'axios';
import { tokenConfig } from './auth';
import { GET_PRODUCT, GET_PRIVATEPRODUCT } from './types';

// GET PRODUCT INFO for displaying one item details
export const getProduct = (id) => (dispatch) => {
    //user loading
    //  dispatch({ type: USER_LOADING });

    axios
        .get(`/api/products/${id}`)
        .then(res => {
            dispatch({
                type: GET_PRODUCT,
                payload: res.data
            });
        }).catch(err => console.log(err));
};

// GET PRIVATE PRODUCT INFO for displaying one item details
export const getPrivateProduct = (id) => (dispatch, getState) => {
    //user loading
    //  dispatch({ type: USER_LOADING });

    axios
        .get(`/api/products-logged/${id}`, tokenConfig(getState))
        .then(res => {
            dispatch({
                type: GET_PRIVATEPRODUCT,
                payload: res.data
            });
        }).catch(err => console.log(err));
};
