import axios from 'axios';
import { USER_LOADED, USER_LOADING, AUTH_ERROR, LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT_SUCCESS, REGISTER_FAIL, REGISTER_SUCCESS, USERUPDATE_SUCCESS, USERUPDATE_FAIL, PSWUPDATE_SUCCESS, PSWUPDATE_FAIL, GET_ERRORS, ALL_READ_SUCCESS, GET_MESSAGES, ONE_READ_SUCCESS } from './types';

// CHECK TOKEN AND LOAD USER
export const loadUser = () => (dispatch, getState) => {
    //user loading
    dispatch({ type: USER_LOADING });


    axios
        .get('/api/auth/user', tokenConfig(getState))
        .then(res => {
            dispatch({
                type: USER_LOADED,
                payload: res.data
            });
        }).catch(err => {
            // dispatch(returnErrors(err.response.data, err.response.status));
            dispatch({
                type: AUTH_ERROR
            });
        });
};

// LOGIN USER
export const login = (email, password) => dispatch => {

    // Headers
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    // Request body
    const body = JSON.stringify({ email, password });

    axios
        .post('/api/auth/login', body, config)
        .then(res => {
            dispatch({
                type: LOGIN_SUCCESS,
                payload: res.data
            });
        }).catch(err => {
            const errors = {
                msg: err.response.data,
                status: err.response.status
            }
            // dispatch(returnErrors(err.response.data, err.response.status));
            dispatch({
                // type: LOGIN_FAIL
                type: GET_ERRORS,
                payload: errors
            });
        });
};

// REGISTER USER
export const register = ({ username, email, password }) => dispatch => {

    // Headers
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    // Request body
    const body = JSON.stringify({ username, email, password });

    axios
        .post('/api/auth/register', body, config)
        .then(res => {
            dispatch({
                type: REGISTER_SUCCESS,
                payload: res.data
            });
        }).catch(err => {
            const errors = {
                msg: err.response.data,
                status: err.response.status
            }
            // dispatch(returnErrors(err.response.data, err.response.status));
            dispatch({
                // type: REGISTER_FAIL
                type: GET_ERRORS,
                payload: errors
            });
        });
};

// LOGOUT USER
export const logout = () => (dispatch, getState) => {

    axios
        .post('/api/auth/logout/', null, tokenConfig(getState))
        .then(res => {
            dispatch({
                type: LOGOUT_SUCCESS
            });
        }).catch(err => {
            // dispatch(returnErrors(err.response.data, err.response.status));
        });
};

// UPDATE USER
export const updateUser = formData => {

    return (dispatch, getState) => {

        // console.log(Object.fromEntries(formData));
        axios
            .patch('/api/auth/update', formData, tokenConfig(getState))
            .then(res => {
                dispatch({
                    type: USERUPDATE_SUCCESS,
                    payload: res.data
                });
            }).catch(err => {
                // dispatch(returnErrors(err.response.data, err.response.status));
                dispatch({
                    type: USERUPDATE_FAIL
                });
            });
    }
};

// SET NEW PASSWORD
// export const updatePassword = ({ old_password, password, password2 }) => {

//     return (dispatch, getState) => {

//         // Request body
//         const body = JSON.stringify({ old_password, password, password2 });

//         axios
//             .post('/api/auth/change', body, tokenConfig(getState))
//             .then(res => {
//                 dispatch({
//                     type: PSWUPDATE_SUCCESS,
//                     payload: old_password, password, password2
//                 });
//             }).catch(err => {
//                 const errors = {
//                     msg: err.response.data,
//                     status: err.response.status
//                 }
//                 // dispatch(returnErrors(err.response.data, err.response.status));
//                 dispatch({
//                     // type: REGISTER_FAIL
//                     type: GET_ERRORS,
//                     payload: errors
//                 });
//             });
//     }
// };

// Mark all messages as read
export const markAllRead = (id) => (dispatch, getState) => {

    const body = JSON.stringify({ id });

    axios
        .post('/api/auth/markallasread', body, tokenConfig(getState))
        .then(res => {
            dispatch({
                type: ALL_READ_SUCCESS,
                payload: res.data
            });
        }).catch(err => {
            // dispatch(returnErrors(err.response.data, err.response.status));
        });
};

// Mark one message as read
export const markOneRead = (id) => (dispatch, getState) => {

    const body = JSON.stringify({ id });

    axios
        .post('/api/auth/markasread', body, tokenConfig(getState))
        .then(res => {
            dispatch({
                type: ONE_READ_SUCCESS,
                payload: res.data
            });
        }).catch(err => {
            // dispatch(returnErrors(err.response.data, err.response.status));
        });
};

// Get user messages
export const getMessages = () => (dispatch, getState) => {

    axios
        .get('/api/auth/messages', tokenConfig(getState))
        .then(res => {
            dispatch({
                type: GET_MESSAGES,
                payload: res.data
            });
        }).catch(err => console.log(err));
};



// HELPER FUNCTION (SETUP CONFIG + TOKEN)
export const tokenConfig = getState => {
    // get token from state
    const token = getState().auth.token;

    // Headers
    const config = {
        headers: {
            'Content-Type': 'application/json',
        }
    };

    // if token, add to headers config
    if (token) {
        config.headers["Authorization"] = `Token ${token}`;
    }
    return config;
}