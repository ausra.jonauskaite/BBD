import axios from 'axios';
import { tokenConfig } from './auth';
import { GET_ITEMS, FETCH_BOOKS, FETCH_GAMES, FETCH_BIKES, FETCH_OTHER } from './types';

// GET ITEM INFO for displaying on home page
export const getItems = () => (dispatch) => {
    //user loading
    //  dispatch({ type: USER_LOADING });

    axios
        .get('/api/products')
        .then(res => {
            dispatch({
                type: GET_ITEMS,
                payload: res.data
            });
        }).catch(err => console.log(err));
};

// GET ITEMS for books page
export const fetchBooks = () => (dispatch) => {

    axios
        .get('/api/products?category=Knygos')
        .then(res => {
            dispatch({
                type: FETCH_BOOKS,
                payload: res.data
            });
        }).catch(err => console.log(err));
};


// GET ITEMS for games page
export const fetchGames = () => (dispatch) => {

    axios
        .get('/api/products?category=Stalo žaidimai')
        .then(res => {
            dispatch({
                type: FETCH_GAMES,
                payload: res.data
            });
        }).catch(err => console.log(err));
};

// GET ITEMS for bikes page
export const fetchBikes = () => (dispatch) => {

    axios
        .get('/api/products?category=Dviračiai / paspirtukai')
        .then(res => {
            dispatch({
                type: FETCH_BIKES,
                payload: res.data
            });
        }).catch(err => console.log(err));
};

// GET ITEMS for other page
export const fetchOther = () => (dispatch) => {

    axios
        .get('/api/products?category=Kitos prekės')
        .then(res => {
            dispatch({
                type: FETCH_OTHER,
                payload: res.data
            });
        }).catch(err => console.log(err));
};
