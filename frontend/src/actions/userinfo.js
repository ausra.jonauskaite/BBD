import axios from 'axios';
import { tokenConfig } from './auth';
import { GET_USERINFO, GET_USERITEMS } from './types';

// GET USERINFO
export const getUserinfo = () => (dispatch, getState) => {
    //user loading
    //  dispatch({ type: USER_LOADING });

    axios
        .get('/api/auth/user', tokenConfig(getState))
        .then(res => {
            dispatch({
                type: GET_USERINFO,
                payload: res.data
            });
        }).catch(err => console.log(err));
};

// GET USER ITEMS
export const getUserItems = () => (dispatch, getState) => {
    //user loading
    //  dispatch({ type: USER_LOADING });

    axios
        .get('/api/products-user', tokenConfig(getState))
        .then(res => {
            dispatch({
                type: GET_USERITEMS,
                payload: res.data
            });
        }).catch(err => console.log(err));
};
