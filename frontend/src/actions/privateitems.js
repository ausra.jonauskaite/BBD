import axios from 'axios';
import { tokenConfig } from './auth';
import { GET_PRIVATEITEMS } from './types';

// GET ITEM INFO for displaying on home page
export const getPrivateItems = () => (dispatch, getState) => {

    axios
        .get('/api/products-logged', tokenConfig(getState))
        .then(res => {
            dispatch({
                type: GET_PRIVATEITEMS,
                payload: res.data
            });
        }).catch(err => console.log(err));
};

