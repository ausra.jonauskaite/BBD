import { GET_PRIVATESEARCH_TERM } from './types';
import axios from 'axios';
import { tokenConfig } from './auth';

// GET PRIVATE SEARCH TERM
export const privateSearch = (term) => (dispatch, getState) => {

    axios
        .get(`/api/products-logged?search=${term}`, tokenConfig(getState))
        .then(res => {
            dispatch({
                type: GET_PRIVATESEARCH_TERM,
                payload: res.data,
                privatename: term
            });
        }).catch(err => console.log(err));
};