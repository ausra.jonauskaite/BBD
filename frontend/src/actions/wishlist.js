import axios from 'axios';
import { tokenConfig } from './auth';
import { GET_WISHLIST, POST_WISHLIST } from './types';

// GET WISHLIST ITEMS
export const getWishlist = () => (dispatch, getState) => {

    axios
        .get('/api/product/user-wishlist', tokenConfig(getState))
        .then(res => {
            dispatch({
                type: GET_WISHLIST,
                payload: res.data
            });
        }).catch(err => console.log(err));
};

// POST TO WISHLIST
export const postWishlist = (id) => (dispatch, getState) => {

    const body = JSON.stringify({ id });

    axios
        .post('/api/product/wishlist', body, tokenConfig(getState))
        .then(res => {
            dispatch({
                type: POST_WISHLIST,
                payload: res.data
            });
        }).catch(err => console.log(err));
};
