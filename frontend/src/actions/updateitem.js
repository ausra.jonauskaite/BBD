import axios from 'axios';
import { tokenConfig } from './auth';
import { LOAD_ITEM, UPDATE_ITEM, DELETE_ITEM } from './types';

// GET ITEM INFO for displaying on home page
export const loadItem = (id) => (dispatch, getState) => {

    axios
        .get(`/api/products-user/${id}`, tokenConfig(getState))
        .then(res => {
            dispatch({
                type: LOAD_ITEM,
                payload: res.data
            });
        }).catch(err => console.log(err));
};

// ITEM UPDATE
// export const updateItem = (formData, id) => {

//     return (dispatch, getState) => {

//         // console.log(Object.fromEntries(formData));
//         // console.log(id);

//         axios
//             .patch(`/api/products-user/${id}`, formData, tokenConfig(getState))
//             .then(res => {
//                 dispatch({
//                     type: UPDATE_ITEM,
//                     payload: res.data
//                 });
//             }).catch(err => console.log(err));
//     }
// };

// DELETE ITEM 
// export const deleteItem = (id) => (dispatch, getState) => {

//     axios
//         .delete(`/api/products-user/${id}`, tokenConfig(getState))
//         .then(res => {
//             dispatch({
//                 type: DELETE_ITEM,
//                 payload: id
//             });
//         }).catch(err => console.log(err));
// };