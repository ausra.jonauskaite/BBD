import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import { HashRouter as Router, Switch, Route, Link, Redirect, withRouter } from "react-router-dom";
import { Provider } from 'react-redux';
import store from './store';

import Helmet from 'react-helmet';
import Navbar from './components/Navbar';
import PrivacyPolicy from './docs/PrivacyPolicy';
import Rules from './docs/Rules';

import PrivateRoute from './components/common/PrivateRoute';
import UnauthorizedRoute from './components/common/UnauthorizedRoute';
import { loadUser } from './actions/auth';

// Transitions
// import { AnimatePresence, motion } from 'framer-motion';

// Private route imports
import PublicLogin from './components/PublicRoutes/PublicLogin';
import PublicRegister from './components/PublicRoutes/PublicRegister';
import PrivateWishlist from './components/PrivateRoutes/PrivateWishlist';
import UserAccount from './components/PrivateRoutes/UserAccount';
import UserUpdate from './components/PrivateRoutes/UserUpdate';
import AdList from './components/PrivateRoutes/AdList';
import UserMap from './components/PrivateRoutes/UserMap';
import UserNotifications from './components/PrivateRoutes/UserNotifications';
import AddItem from './components/PrivateRoutes/AddItem';
import ScrollToTop from './components/ScrollToTop';
import ProductRoute from './components/PublicRoutes/ProductRoute';
import PrivateProductRoute from './components/PrivateRoutes/PrivateProductRoute';
import UserAdModification from './components/PrivateRoutes/ModifyAdList';
import MainPage from './components/PublicRoutes/MainPage';
import BooksCategory from './components/PublicRoutes/BooksCategory';
import GamesCategory from './components/PublicRoutes/GamesCategory';
import BicycleCategory from './components/PublicRoutes/BicycleCategory';
import OtherCategory from './components/PublicRoutes/OtherCategory';
import PrivateSearch from './components/PrivateRoutes/PrivateSearch';
import ItemUpdate from './components/PrivateRoutes/ItemUpdate';
import ItemsSearch from './components/PublicRoutes/ItemsSearch';
import ModifyItem from './components/ModifyItem';
import ItemUploaded from './components/ItemUploaded';
import ItemError from './components/ItemError';
import PageNotFound from './components/PageNotFound';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    };
  }

  componentDidMount() {
    store.dispatch(loadUser());
    const { isLoading } = this.state;
    this.setState({ isLoading: false });
  }

  render() {

    const { isLoading } = this.state;
    if (isLoading) {
      return <div>LOADING...</div>;
    }

    return (
      <Provider store={store}>
        <Router >
          <ScrollToTop>
            <div className="app">
              <Switch>
                {/* PUBLIC ROUTES */}

                {/* LOGIN */}
                <Route path="/prisijungimas" >
                  <Navbar />
                  <PublicLogin />
                </Route>

                {/* REGISTER */}
                <Route path="/registracija">
                  <Navbar />
                  <PublicRegister />
                </Route>

                {/* PUBLIC CATEGORY PAGES */}
                <Route path="/knygos">
                  <BooksCategory />
                </Route>

                <Route path="/stalo_zaidimai">
                  <GamesCategory />
                  <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
                </Route>

                <Route path="/dviraciai_paspirtukai">
                  <BicycleCategory />
                  <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
                </Route>

                <Route path="/kitos_prekes">
                  {/* <Navbar /> */}
                  <OtherCategory />
                  <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
                </Route>

                {/* DEFAULT ROUTE */}
                <Route exact path="/">
                  <Navbar />
                  <MainPage />
                </Route>

                {/* PRIVATE ROUTES */}
                <PrivateRoute path="/profilis" component={UserAccount} />
                <PrivateRoute path="/paskyros_redagavimas" component={UserUpdate} />
                <PrivateRoute path="/pazymetos_prekes" component={PrivateWishlist} />
                <PrivateRoute path="/mano_skelbimai" component={AdList} />
                <PrivateRoute path="/zemelapis" component={UserMap} />
                <PrivateRoute path="/pranesimai" component={UserNotifications} />
                <PrivateRoute path="/ikelti_preke" component={AddItem} />
                <PrivateRoute path="/redaguoti/:item_id" component={ModifyItem} />
                <PrivateRoute path="/prekes_redagavimas/:item_id" component={ItemUpdate} />
                <PrivateRoute path="/redaguoti_skelbimus" component={UserAdModification} />
                <PrivateRoute path="/preke_/:item_id" component={PrivateProductRoute} />
                <PrivateRoute path="/products-logged?search=:term" component={PrivateSearch} />
                <PrivateRoute path="/preke_ikelta" component={ItemUploaded} />
                <PrivateRoute path="/klaida" component={ItemError} />

                {/* RULES AND PRIVACY POLICY */}
                <UnauthorizedRoute exact path="/privatumo_politika" component={PrivacyPolicy} />

                <UnauthorizedRoute exact path="/taisykles" component={Rules} />

                {/* PUBLIC SEARCH */}
                <Route path="/products?search=:term" component={ItemsSearch} />

                {/* ITEM ROUTE */}
                <UnauthorizedRoute path="/preke/:item_id" component={ProductRoute} />

                {/* 404 page not found */}
                <Route path="/404" component={PageNotFound} />
                <Redirect from="*" to="/404" />

              </Switch>
            </div>
          </ScrollToTop>
        </Router>
      </Provider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));