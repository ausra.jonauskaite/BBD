import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import "./Login.css";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { login } from '../actions/auth';


export class Login extends Component {

    state = {
        email: "",
        password: "",
        emailError: "",
        passwordError: "",
        blankError: false,
    };

    componentDidMount() { }

    static propTypes = {
        login: PropTypes.func.isRequired,
        isAuthenticated: PropTypes.bool,
        error: PropTypes.object.isRequired
    }

    handleEmailChange = event => {
        this.setState({ email: event.target.value }, () => {
            this.validateEmail();
        });
    };

    validateEmail = () => {
        const { email } = this.state;

        if (email.length == 0) {
            this.setState({ emailError: 'Laukelis negali būti tuščias' });
        } else {
            this.setState({ emailError: '' });
        }

        if (!email.includes('@')) {
            this.setState({ emailError: 'Neteisingas el. pašto adresas' });
        } else {
            this.setState({ emailError: '' });
        }

    }

    handlePswChange = event => {
        this.setState({ password: event.target.value }, () => {
            this.validatePsw();
        });
    };

    validatePsw = () => {
        const { password } = this.state;
        if (password.length < 6 || password.length > 24 || password.length == 0) {
            this.setState({ passwordError: 'Slaptažodį turi sudaryti nuo 6 iki 24 simbolių' });
        } else {
            this.setState({ passwordError: '' });
        }

    }

    onSubmit = e => {
        e.preventDefault();
        if (this.state.email == 0 || this.state.password == 0) {
            this.setState({ blankError: true });
            setTimeout(function () { this.setState({ blankError: false }) }.bind(this), 4000);
        } else {
            this.props.login(this.state.email, this.state.password);
        }
    };

    componentDidUpdate(prevProps) {
        const { error } = this.props;
        if (error !== prevProps.error) {
            this.setState({ blankError: true });
            setTimeout(function () { this.setState({ blankError: false }) }.bind(this), 4000);
        }
    }

    render() {

        if (this.props.isAuthenticated) {
            return <Redirect to="/" />
        }

        const { email, password } = this.state;

        return (
            <div>

                {this.state.blankError ? (<div className="alertMsg_container fade-in" ><div className="alert_msg">
                    <p className="msg_cont">Klaida! Neteisingai nurodyti duomenys</p>
                </div></div>) : ''}


                <div className="Login">
                    <div className="Login_form">
                        <Form onSubmit={this.onSubmit}>
                            <h2 className="login_headline">Prisijungimas</h2>
                            <input type="email" className="email_input" placeholder="El. paštas" name="email" value={email} onChange={this.handleEmailChange} onBlur={this.validateEmail} />
                            <div className='input_errors'>{this.state.emailError}</div>
                            <input type="password" className="psw_input" placeholder="Slaptažodis" name="password" value={password} onChange={this.handlePswChange} onBlur={this.validatePsw} />
                            <div className='input_errors'>{this.state.passwordError}</div>
                            <h5 id="forgot_psw" className="login_msg">Pamiršote slaptažodį?</h5>
                            <button className="login_btn" type="submit" onSubmit={this.onSubmit}>
                                Prisijungti
                </button>
                            <h5 className="login_msg">Neturite paskyros?
                    <Link to='/registracija' className="login_msg" id="footer_margin"> Susikurkite!</Link> </h5>
                        </Form>

                    </div>
                </div>
                {/* Rules section */}
                <div className="footer_position">
                    <div className="footer_divider"></div>
                    <Link to="/privatumo_politika" className="link_style"><p className="footer_msg">Privatumo politika</p></Link>
                    <Link to="/taisykles" className="link_style"><p className="footer_msg">Taisyklės</p></Link>
                </div>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    error: state.errors
});

export default connect(mapStateToProps, { login })(Login);

