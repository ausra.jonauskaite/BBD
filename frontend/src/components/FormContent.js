import React from 'react';
import './FormContent.css';
import { useState } from 'react';
import { Component } from 'react';
import store from '../store';
import { tokenConfig } from '../actions/auth';
import axios from 'axios';
import PropTypes from "prop-types";
import Form from "react-bootstrap/Form";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";


class FormContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            description: '',
            selected: '',
            primary_value: '',
            address: '',
            for_sale: '',
            price: '',
            image: '',
            imgPreview: null,
            isChecked: '',
            value: '',
            setSelected: '',
            categoryAlert: '',
            categoryLoading: '',
            titleError: '',
            dboxError: '',
            dboxLoading: false,
            dboxDetail: '',
            stateError: '',
            categoryError: '',
            primaryValueError: '',
            isChecked: false,
            checkedAddress: true,
            priceError: '',
            addressError: '',
            blankError: '',
            updateSuccess: '',
            status: ''
        };
        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleDboxChange = this.handleDboxChange.bind(this);
        this.searchCategory = this.searchCategory.bind(this);
        this.handleCategoryInput = this.handleCategoryInput.bind(this);
        this.handleStateInput = this.handleStateInput.bind(this);
        this.handleCheckboxInput = this.handleCheckboxInput.bind(this);
        this.handleAddressChange = this.handleAddressChange.bind(this);
        this.handleAddressBox = this.handleAddressBox.bind(this);
    }

    // Image Change
    handleImageChange(event) {
        this.setState({
            image: event.target.files[0],
            imgPreview: URL.createObjectURL(event.target.files[0])
        })
    };

    // Fetch category from API
    fetchCategory(formData) {
        axios
            .post('/api/class', formData, tokenConfig(store.getState))
            .then(response => { this.setState({ setSelected: response.data.category, categoryLoading: false, status: response.status }) })
            .catch(err => { this.setState({ status: err.response.status }) });
    }

    // Searching category
    searchCategory(e) {
        e.preventDefault();
        const imgPrev = this.state.imgPreview;
        if (imgPrev) {
            const formData = new FormData();
            formData.append('imageFile', this.state.image);
            this.setState({ categoryLoading: true });
            this.fetchCategory(formData);
        } else {
            this.setState({ categoryAlert: 'Klaida! Įkelkite nuotrauką' });
        }
    }

    // Handle category input
    handleCategoryInput(event) {
        this.setState({
            setSelected: event.target.value,
            categoryError: ''
        });
    }

    // Handle title input
    handleTitleChange = event => {
        this.setState({ name: event.target.value, readOnlyAll: false }, () => {
            this.validateTitle();
        });
    };

    validateTitle = () => {
        const { name } = this.state;

        if (name.length < 4 || name.length > 60 || name.length == 0) {
            this.setState({ titleError: 'Pavadinimą turi sudaryti bent 4 simboliai' });
        } else {
            this.setState({ titleError: '' });
        }
    };

    // Handle DBOX input
    handleDboxChange = event => {
        this.setState({ description: event.target.value }, () => {
            this.validateDbox();
        });
    };

    validateDbox = () => {
        const { description } = this.state;
        if (description.length == 0) {
            this.setState({ dboxError: 'Aprašymas negali būti tuščias' });
        } else {
            this.setState({ dboxError: '' });
        }
    };

    // Get description
    handleDboxBtn = event => {
        event.preventDefault();
        const formData = new FormData();
        formData.append('name', this.state.name);
        formData.append('category', this.state.setSelected);
        this.setState({ dboxLoading: true });
        this.fetchDbox(formData);
    }

    fetchDbox(formData) {
        axios
            .post('/api/product/get_description', formData, tokenConfig(store.getState))
            .then(response => { this.setState({ description: response.data.description, dboxLoading: false, dboxDetail: response.data.detail, status: response.status }) })
            .catch(err => { this.setState({ status: err.response.status }) });
    }

    handleClear = e => {
        e.preventDefault();
        this.setState({ description: '' });
    }

    // Handle State 
    handleStateInput(event) {
        this.setState({
            selected: event.target.value,
            stateError: ''
        });
    };

    // Primary value validation
    handlePrimaryChange = event => {
        const primary_value = event.target.value;
        if (!primary_value || primary_value.match(/^\d{1,}(\.\d{0,2})?$/)) {
            this.setState({ primary_value }, () => {
                this.validatePrimary();
            });
        }
    };

    validatePrimary = () => {
        const { primary_value } = this.state;

        if (primary_value.length == 0 || primary_value > 1000000) {
            this.setState({ primaryValueError: 'Neteisingai nurodyta pradinė prekė vertė' });
        } else {
            this.setState({ primaryValueError: '' });
        }
    };

    // Address validation
    handleAddressChange = event => {
        this.setState({ address: event.target.value }, () => {
            this.validateAddress();
        });
    };

    validateAddress = () => {
        const { address } = this.state;

        if (address.length < 6 || address.length > 50 || address.length == 0) {
            this.setState({ addressError: 'Neteisingas nurodytas adresas' });
        } else {
            this.setState({ addressError: '' });
        }
    };

    handleCheckboxInput() {
        this.setState({
            isChecked: !this.state.isChecked
        });
    };

    // Price validation
    handlePriceChange = event => {
        const price = event.target.value;
        if (!price || price.match(/^\d{1,}(\.\d{0,2})?$/)) {
            this.setState({ price }, () => {
                this.validatePrice();
            });
        }
    };

    validatePrice = () => {
        const { price } = this.state;

        if (price.length == 0 || price > 1000000) {
            this.setState({ priceError: 'Neteisingai nurodyta prekės kaina' });
        } else {
            this.setState({ priceError: '' });
        }
    };

    handleAddressBox() {
        this.setState({
            checkedAddress: !this.state.checkedAddress
        });
    }


    finalValidation = () => {

        let imageError = "";
        let categoryError = "";
        let titleError = "";
        let dboxError = "";
        let stateError = "";
        let valueError = "";
        let addressError = "";
        let priceError = "";

        // image validation
        if (this.state.image === undefined || this.state.image === '' || this.state.image === null) {
            imageError = 'Nera nuotraukos';
        }

        if (imageError) {
            return false;
        }

        // category validation
        if (this.state.setSelected === undefined || this.state.setSelected === '' || this.state.setSelected === 'pasirinkite kategorija') {
            categoryError = 'Nera kategorijos';
        }

        if (categoryError) {
            return false;
        }

        // title validation
        if (this.state.name === undefined || this.state.name === '' || this.state.name.length < 4) {
            titleError = 'Nera pavadinimo';
        }

        if (titleError) {
            return false;
        }

        // dbox validation
        if (this.state.description === undefined || this.state.description === '' || this.state.description.length === 0) {
            dboxError = 'Nera aprasymo';
        }

        if (dboxError) {
            return false;
        }

        // state validation
        if (this.state.selected === undefined || this.state.selected === '' || this.state.selected === 'pasirinkite bukle') {
            stateError = 'Nera bukles';
        }

        if (stateError) {
            return false;
        }

        // primary value validation
        if (this.state.primary_value === undefined || this.state.primary_value === '' || this.state.primary_value === 0) {
            valueError = 'Nera pradines vertes';
        }

        if (valueError) {
            return false;
        }

        //address validation
        if ((this.state.checkedAddress === true && (this.state.address.length !== 0 || this.props.user.address === '' || this.props.user.address === undefined)) || (this.state.checkedAddress === false && this.state.address.length === 0)) {
            addressError = 'Nera adreso';
        }

        if (addressError) {
            return false;
        }

        //price validation
        if ((this.state.isChecked === true && this.state.price.length === 0) || (this.state.isChecked === false && this.state.price.length !== 0)) {
            priceError = 'Nera kainos';
        }

        if (priceError) {
            return false;
        }

        return true;

    }

    // SUBMIT FORM
    onSubmit = (event) => {

        event.preventDefault();

        // validation
        const isValid = this.finalValidation();

        if (isValid) {

            // const img = this.state.image;
            const checkAddress = this.state.checkedAddress;
            const userAddress = this.props.user.address;


            if (checkAddress) {
                const default_price = "0";
                const formData = new FormData();
                // formData.append('id', this.state.id);
                formData.append('image', this.state.image);
                formData.append('category', this.state.setSelected);
                formData.append('name', this.state.name);
                formData.append('description', this.state.description);
                formData.append('state', this.state.selected);
                formData.append('primary_value', this.state.primary_value);
                formData.append('location', userAddress);


                if (this.state.isChecked === true) {
                    formData.append('for_sale', this.state.isChecked);
                    formData.append('price', this.state.price);
                } else {
                    formData.append('for_sale', this.state.isChecked);
                    formData.append('price', default_price);
                }

                this.uploadItem(formData);
                // console.log(Object.fromEntries(formData));

            } else {
                const def_price = "0";
                const formData = new FormData();
                // formData.append('id', this.state.id);
                formData.append('category', this.state.setSelected);
                formData.append('name', this.state.name);
                formData.append('image', this.state.image);
                formData.append('description', this.state.description);
                formData.append('state', this.state.selected);
                formData.append('primary_value', this.state.primary_value);
                formData.append('location', this.state.address);

                if (this.state.isChecked === true) {
                    formData.append('for_sale', this.state.isChecked);
                    formData.append('price', this.state.price);
                } else {
                    formData.append('for_sale', this.state.isChecked);
                    formData.append('price', def_price);
                }

                this.uploadItem(formData);
                // console.log(Object.fromEntries(formData));
            }
        } else {
            this.setState({ blankError: true });
            setTimeout(function () { this.setState({ blankError: false }) }.bind(this), 4000);
        }

    };

    uploadItem(formData) {
        axios
            .post('/api/product/create', formData, tokenConfig(store.getState))
            .then(response => { this.setState({ updateSuccess: response.data, status: response.status }) })
            .catch(err => { this.setState({ status: err.response.status }) });
    }

    render() {

        const { name, primary_value, price, address, imgPreview } = this.state;

        if (this.state.status === 200) {
            return <Redirect to="/preke_ikelta" />
        }

        if (this.state.status === 500) {
            return <Redirect to="/klaida" />
        }

        return (
            <div>
                <div className="sticky-container form_container">
                    <div className="error_container">
                        {this.state.blankError || this.state.status === 400 ? (<div className="alertMsg_container fade-in" ><div className="alert_msg">
                            <p className="msg_cont">Klaida! Neteisingai nurodyti duomenys</p>
                        </div></div>) : ''}
                    </div>
                </div>

                <div className="form_mainContainer">
                    <div className="form_container">
                        <h2 className="form_headline">Įkelti prekę</h2>
                        <div className="form_box">
                            <Form className="form_content" onSubmit={this.onSubmit.bind(this)}>

                                <div className="image_container">
                                    {this.state.imgPreview ?
                                        <img src={imgPreview} alt="" />
                                        : ''}
                                </div>

                                {this.state.imgPreview ? '' : (<div><p className="input_errors category-alert">{this.state.categoryAlert}</p></div>)}
                                {/* {this.state.categoryLoading ? (<div><p className="category-input category-alert">Prašome palaukti. Vykdomas kategorijos atpažinimas</p></div>) : ''} */}
                                {this.state.categoryLoading ? (<div className="center-loader">
                                    <div className="sk-circle">
                                        <div className="sk-circle1 sk-child"></div>
                                        <div className="sk-circle2 sk-child"></div>
                                        <div className="sk-circle3 sk-child"></div>
                                        <div className="sk-circle4 sk-child"></div>
                                        <div className="sk-circle5 sk-child"></div>
                                        <div className="sk-circle6 sk-child"></div>
                                        <div className="sk-circle7 sk-child"></div>
                                        <div className="sk-circle8 sk-child"></div>
                                        <div className="sk-circle9 sk-child"></div>
                                        <div className="sk-circle10 sk-child"></div>
                                        <div className="sk-circle11 sk-child"></div>
                                        <div className="sk-circle12 sk-child"></div>
                                    </div>
                                </div>) : ''}

                                <div className="image_upload2">
                                    <input className="file" type="file" id="img_file" accept="image/png, image/jpeg, image/jpg" name="image" onChange={this.handleImageChange} />
                                    <label htmlFor="img_file"><span className="material-icons upload-pic">upload_file</span></label>
                                    <button onClick={this.searchCategory} className="search-category">Ieškoti kategorijos</button>
                                </div>

                                <div className="formats_msg">
                                    <p>Palaikomi nuotraukų formatai: JPEG, JPG, PNG</p>
                                    <p>Dydis: 1280x720</p>
                                </div>

                                <div className="section_divider"></div>

                                <div className="dbox_container">
                                    <label className="label_title">
                                        Kategorija
                        </label>
                                    <select className="option_selection" value={this.state.setSelected} onChange={this.handleCategoryInput}>
                                        <option value="pasirinkite kategorija">Pasirinkite kategoriją</option>
                                        <option value="Knygos">Knygos</option>
                                        <option value="Stalo žaidimai">Stalo žaidimai</option>
                                        <option value="Dviračiai / paspirtukai">Dviračiai / paspirtukai</option>
                                        <option value="Kitos prekės">Kitos prekės</option>
                                    </select>


                                    <label className="label_title">
                                        Pavadinimas
                        </label>
                                    <input type="text" className="input_container" name="name" value={name} onChange={this.handleTitleChange} onBlur={this.validateTitle} />
                                    <div className='input_errors'>{this.state.titleError}</div>


                                    <label className="label_title">
                                        Aprašymas
                        </label>
                                    {(this.state.setSelected === 'Knygos' || this.state.setSelected === 'Stalo žaidimai') && name ? (<div className="switch-container">
                                        <div className="search_f">
                                            <span className="material-icons md-24 filter_button" onClick={this.handleDboxBtn}>search</span>
                                        </div>

                                        {this.state.description ? (<div className="search_f">
                                            <span className="material-icons md-24 filter_button" onClick={this.handleClear}>delete_forever</span>
                                        </div>) : ''}
                                    </div>) : ''}

                                    {this.state.dboxLoading ? (<div className="center-loader-2">
                                        <div className="sk-circle">
                                            <div className="sk-circle1 sk-child"></div>
                                            <div className="sk-circle2 sk-child"></div>
                                            <div className="sk-circle3 sk-child"></div>
                                            <div className="sk-circle4 sk-child"></div>
                                            <div className="sk-circle5 sk-child"></div>
                                            <div className="sk-circle6 sk-child"></div>
                                            <div className="sk-circle7 sk-child"></div>
                                            <div className="sk-circle8 sk-child"></div>
                                            <div className="sk-circle9 sk-child"></div>
                                            <div className="sk-circle10 sk-child"></div>
                                            <div className="sk-circle11 sk-child"></div>
                                            <div className="sk-circle12 sk-child"></div>
                                        </div>
                                    </div>) : ''}

                                    {this.state.dboxDetail ? (<div className='input_errors'>{this.state.dboxDetail}</div>) : ''}

                                    <textarea className="desc_box" placeholder="Prekės aprašymas" name="description" value={this.state.description} onChange={this.handleDboxChange} onBlur={this.validateDbox} />
                                    <div className='input_errors'>{this.state.dboxError}</div>
                                </div>

                                <div className="section_divider"></div>

                                <div className="categorybox_container">


                                    <label className="label_title">
                                        Būklė
                        </label>
                                    <select className="option_selection" onChange={this.handleStateInput} value={this.state.selected}>
                                        <option value="pasirinkite bukle">Pasirinkite būklę</option>
                                        <option value="Nauja">Nauja</option>
                                        <option value="Labai gera">Labai gera</option>
                                        <option value="Gera">Gera</option>
                                        <option value="Patenkinama">Patenkinama</option>
                                    </select>

                                    <label className="label_title">
                                        Pradinės prekės vertė
                        </label>
                                    <input type="text" className="input_container" name="primary_value" value={primary_value} onChange={this.handlePrimaryChange} onBlur={this.validatePrimary} />
                                    <div className='input_errors'>{this.state.primaryValueError}</div>
                                    <div className="section_divider"></div>

                                    <label className="label_title">
                                        Adresas
                            </label>
                                    <div className="address_container">
                                        <p>Įkelti iš mano paskyros*</p>
                                        <input type="checkbox" className="checkbox" name="address" value="address" defaultChecked={this.state.checkedAddress} onChange={this.handleAddressBox} />
                                    </div>

                                    {this.state.checkedAddress ? (<p className="side-note">*Pastaba: adresas privalo būti nurodytas vartotojo paskyroje</p>) : ''}

                                    {this.state.checkedAddress ? '' : (<div>
                                        <label className="label_title">
                                            Įvesti adresą
                            </label>
                                        <input type="text" className="input_container" name="address" value={address} onChange={this.handleAddressChange} onBlur={this.validateAddress} />
                                        <div className='input_errors'>{this.state.addressError}</div>
                                    </div>)}


                                </div>

                                <div className="section_divider"></div>

                                <div className="sell_container">
                                    <label className="label_title">
                                        Domina pardavimas
                    </label>
                                    <input type="checkbox" className="checkbox-sale" name="for_sale" value="for_sale" defaultChecked={this.state.isChecked} onChange={this.handleCheckboxInput} />
                                </div>

                                {this.state.isChecked ? (<div><label className="label_title">
                                    Kaina
                        </label>
                                    <input type="text" className="input_container" name="price" value={price} onChange={this.handlePriceChange} onBlur={this.validatePrice} /> <div className='input_errors'>{this.state.priceError}</div> </div>) : ('')}

                                <div className="btn_position">
                                    <Link to="/"><button className="cancelation_btn">Atšaukti</button></Link>
                                    <button className="validation_btn" type="submit">Įkelti</button>
                                </div>

                            </Form>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

const mapStateToProps = state => ({
    user: state.auth.user
});

export default connect(mapStateToProps)(FormContent)
