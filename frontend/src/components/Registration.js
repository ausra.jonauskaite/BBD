import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import "./Registration.css";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { register } from '../actions/auth';

export class Registration extends Component {

    state = {
        username: "",
        email: "",
        password: "",
        passwordConf: "",
        termsConditions: true,
        nameError: "",
        emailError: "",
        passwordError: "",
        passwordConfError: "",
        blankError: false,
        takenEmailError: false
    };

    static propTypes = {
        register: PropTypes.func.isRequired,
        isAuthenticated: PropTypes.bool,
        error: PropTypes.object.isRequired
    }

    componentDidMount() { }

    componentDidUpdate(prevProps) {
        const { error } = this.props;
        if (error !== prevProps.error) {
            this.setState({ takenEmailError: true });
            setTimeout(function () { this.setState({ takenEmailError: false }) }.bind(this), 4000);
            // console.log('checking');
        }
    }

    onChange = (e) => this.setState({ [e.target.name]: e.target.value });

    // Username validation
    handleNameChange = event => {
        this.setState({ username: event.target.value }, () => {
            this.validateName();
        });
    };

    validateName = () => {
        const { username } = this.state;

        if (username.length == 0) {
            this.setState({ nameError: 'Laukelis negali būti tuščias' });
        } else {
            this.setState({ nameError: '' });
        }

        if (username.length < 5 || username.length > 24) {
            this.setState({ nameError: 'Vardą turi sudaryti nuo 6 iki 24 simbolių' });
        } else {
            this.setState({ nameError: '' });
        }
    }

    // handleNameChange = event => {
    //     this.setState({ username: event.target.value }, () => {
    //         this.validateName();
    //     });
    // };

    // Email validation
    handleEmailChange = event => {
        this.setState({ email: event.target.value }, () => {
            this.validateEmail();
        });
    };

    validateEmail = () => {
        const { email } = this.state;

        if (email.length == 0) {
            this.setState({ emailError: 'Laukelis negali būti tuščias' });
        } else {
            this.setState({ emailError: '' });
        }

        if (!email.includes('@')) {
            this.setState({ emailError: 'Neteisingas el. pašto adresas' });
        } else {
            this.setState({ emailError: '' });
        }
    }

    // Password validation
    handlePswChange = event => {
        this.setState({ password: event.target.value }, () => {
            this.validatePsw();
        });
    };

    validatePsw = () => {
        const { password } = this.state;
        if (password.length < 6 || password.length > 24 || password.length == 0) {
            this.setState({ passwordError: 'Slaptažodį turi sudaryti nuo 6 iki 24 simbolių' });
        } else {
            this.setState({ passwordError: '' });
        }
    }

    // PasswordConf validation
    handleConfChange = event => {
        this.setState({ passwordConf: event.target.value }, () => {
            this.validateConf();
        });
    };

    validateConf = () => {
        const { passwordConf, password } = this.state;

        if (passwordConf !== password || passwordConf.length < 6 || passwordConf.length > 24 || passwordConf.length == 0) {
            this.setState({ passwordConfError: 'Neteisingai įvestas slaptažodis' });
        } else {
            this.setState({ passwordConfError: '' });
        }
    }

    // Submit form
    onSubmit = (e) => {
        e.preventDefault();
        const { username, email, password, passwordConf, termsConditions } = this.state;

        if (password !== passwordConf || username == 0 || email == 0 || password == 0 || passwordConf == 0 || termsConditions == false) {
            this.setState({ blankError: true });
            setTimeout(function () { this.setState({ blankError: false }) }.bind(this), 4000);
        } else {
            const newUser = {
                email,
                username,
                password
            }
            this.props.register(newUser);
        }
    };


    render() {

        if (this.props.isAuthenticated) {
            return <Redirect to="/prisijungimas" />
        }

        const { username, email, password, passwordConf, termsConditions } = this.state;

        return (
            <div>
                {this.state.blankError ? (<div className="alertMsg_container fade-in" ><div className="alert_msg">
                    <p className="msg_cont">Klaida! Neteisingai nurodyti duomenys</p>
                </div></div>) : ''}

                {this.state.takenEmailError ? (<div className="alertMsg_container fade-in" ><div className="alert_msg">
                    <p className="msg_cont">Klaida! El. paštas jau registruotas sistemoje</p>
                </div></div>) : ''}

                <div className="Register">
                    <div className="Register_form">
                        <Form onSubmit={this.onSubmit}>
                            <h2 className="register_headline">Registracija</h2>
                            <input type="text" className="info_input" placeholder="Vartotojo vardas" name="username" value={username} onChange={this.handleNameChange} onBlur={this.validateName} />
                            <div className='input_errors'>{this.state.nameError}</div>
                            <input type="email" className="info_input" placeholder="El. paštas" name="email" value={email} onChange={this.handleEmailChange} onBlur={this.validateEmail} />
                            <div className='input_errors'>{this.state.emailError}</div>
                            <input type="password" className="info_input" placeholder="Slaptažodis" name="password" value={password} onChange={this.handlePswChange} onBlur={this.validatePsw} />
                            <div className='input_errors'>{this.state.passwordError}</div>
                            <input type="password" className="info_input" placeholder="Slaptažodžio patvirtinimas" name="passwordConf" value={passwordConf} onChange={this.handleConfChange} onBlur={this.validateConf} />
                            <div className='input_errors'>{this.state.passwordConfError}</div>

                            <div className="rules_container">
                                <input type="checkbox" className="rules_msg" name="termsConditions" value={termsConditions} onChange={this.onChange} required="required" defaultChecked={this.state.termsConditions}></input>
                                <p className="rules_msg">Patvirtinu, kad susipažinau ir sutinku su&nbsp;
                    <Link to='/taisykles' className="register_msg">taisyklėmis</Link> ir <Link to='/privatumo_politika' className="register_msg">privatumo politika</Link>. Patvirtinu, kad man ne mažiau kaip 18 metų.</p>
                            </div>

                            <button className="register_btn" type="submit" onSubmit={this.onSubmit} >
                                Registruotis
                </button>

                            <h5 className="register_msg">Reikia pagalbos?</h5>
                        </Form>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    error: state.errors
});

export default connect(mapStateToProps, { register })(Registration);


