import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from "prop-types";


const UnauthorizedRoute = ({ component: Component, auth, ...rest }) => (
    <Route
        {...rest}
        render={props => {
            if (auth.isAuthenticated) {
                return <Redirect
                    to={{ pathname: "/", state: { from: props.location } }}
                />
            }
            else {
                return <Component {...props} />
            }
        }}
    />
);

UnauthorizedRoute.propTypes = {
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(UnauthorizedRoute);


