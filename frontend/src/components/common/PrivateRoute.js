import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from "prop-types";
import LoadingPage from '../LoadingPage';

const PrivateRoute = ({ component: Component, auth, ...rest }) => (
    <Route
        {...rest}
        render={props => {
            if (auth.isLoading) {
                return <LoadingPage />
            } else if (auth.isAuthenticated) {
                return <Component {...props} />
            }
            else {
                return <Redirect
                    to={{ pathname: "/prisijungimas", state: { from: props.location } }}
                />
            }
        }}
    />
);

PrivateRoute.propTypes = {
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(PrivateRoute);
