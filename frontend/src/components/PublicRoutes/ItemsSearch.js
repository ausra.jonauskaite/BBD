import React, { Component } from 'react';
import Helmet from 'react-helmet';
import PublicSearch from '../PublicSearch';
import Navbar from '../Navbar';


export class ItemsSearch extends Component {

    render() {
        return (
            <div>
                <Navbar />
                <PublicSearch />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default ItemsSearch
