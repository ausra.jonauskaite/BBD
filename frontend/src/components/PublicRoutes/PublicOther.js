import React, { Component } from 'react';
import Navbar from "../Navbar";
import AllItems from "../AllItems";
import Helmet from 'react-helmet';

export class PublicOther extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <AllItems />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default PublicOther