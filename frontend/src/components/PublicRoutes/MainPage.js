import React, { Component } from 'react'
import PublicHome from './PublicHome';
import PrivateHome from '../PrivateRoutes/PrivateHome';
import { connect } from "react-redux";
import PropTypes from "prop-types";



class MainPage extends Component {

    static propTypes = {
        auth: PropTypes.object.isRequired
    }

    render() {

        const { isAuthenticated, user } = this.props.auth;

        const authHome = (<PrivateHome />);
        const guestHome = (<PublicHome />);

        return (
            <div>
                { isAuthenticated ? authHome : guestHome}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(MainPage)