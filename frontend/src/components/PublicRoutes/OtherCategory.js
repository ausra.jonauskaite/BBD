import React, { Component } from 'react'
import PublicOther from './PublicOther';
import PrivateOther from './PrivateOther';
import { connect } from "react-redux";
import PropTypes from "prop-types";



class OtherCategory extends Component {

    static propTypes = {
        auth: PropTypes.object.isRequired
    }

    render() {

        const { isAuthenticated, user } = this.props.auth;

        const authOther = (<PrivateOther />);
        const guestOther = (<PublicOther />);

        return (
            <div>
                { isAuthenticated ? authOther : guestOther}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(OtherCategory)
