import React, { Component } from 'react';
import Navbar from "../Navbar";
import GamesItems from "../GamesItems";
import Helmet from 'react-helmet';

export class PublicGames extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <GamesItems />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default PublicGames