import React, { Component } from 'react'
import PublicBooks from './PublicBooks';
import PrivateBooks from './PrivateBooks';
import { connect } from "react-redux";
import PropTypes from "prop-types";



class BooksCategory extends Component {

    static propTypes = {
        auth: PropTypes.object.isRequired
    }

    render() {

        const { isAuthenticated, user } = this.props.auth;

        const authBooks = (<PrivateBooks />);
        const guestBooks = (<PublicBooks />);

        return (
            <div>
                { isAuthenticated ? authBooks : guestBooks}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(BooksCategory)
