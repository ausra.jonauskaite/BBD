import React, { Component } from 'react'
import PublicBicycle from './PublicBicycle';
import PrivateBicycle from './PrivateBicycle';
import { connect } from "react-redux";
import PropTypes from "prop-types";



class BicycleCategory extends Component {

    static propTypes = {
        auth: PropTypes.object.isRequired
    }

    render() {

        const { isAuthenticated, user } = this.props.auth;

        const authBicycle = (<PrivateBicycle />);
        const guestBicycle = (<PublicBicycle />);

        return (
            <div>
                { isAuthenticated ? authBicycle : guestBicycle}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(BicycleCategory)
