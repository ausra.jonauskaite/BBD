import React, { Component } from 'react';
import Navbar from "../Navbar";
import BooksItemsPrivate from "../BooksItemsPrivate";
import Helmet from 'react-helmet';

export class PrivateBooks extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <BooksItemsPrivate />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default PrivateBooks