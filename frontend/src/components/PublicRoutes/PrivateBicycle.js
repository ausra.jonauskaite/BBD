import React, { Component } from 'react';
import Navbar from "../Navbar";
import BicycleItemsPrivate from "../BicycleItemsPrivate";
import Helmet from 'react-helmet';

export class PrivateBicycle extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <BicycleItemsPrivate />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default PrivateBicycle