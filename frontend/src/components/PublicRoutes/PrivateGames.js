import React, { Component } from 'react';
import Navbar from "../Navbar";
import GamesItemsPrivate from "../GamesItemsPrivate";
import Helmet from 'react-helmet';

export class PrivateGames extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <GamesItemsPrivate />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default PrivateGames