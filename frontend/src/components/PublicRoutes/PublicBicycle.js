import React, { Component } from 'react';
import Navbar from "../Navbar";
import BicycleItems from "../BicycleItems";
import Helmet from 'react-helmet';

export class PublicBicycle extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <BicycleItems />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default PublicBicycle