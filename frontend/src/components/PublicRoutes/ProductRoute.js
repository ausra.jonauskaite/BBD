import React, { Component } from 'react';
import ExpandProduct from "../ExpandProduct";
import Helmet from 'react-helmet';
import Navbar from '../Navbar';


export class ProductRoute extends Component {

    constructor(props) {
        super(props);

    };
    componentDidMount() {
        const id = this.props.match.params.item_id;
    }

    render() {
        return (
            <div>
                <Navbar />
                <ExpandProduct />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default ProductRoute
