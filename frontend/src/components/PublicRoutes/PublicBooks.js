import React, { Component } from 'react';
import Navbar from "../Navbar";
import BooksItems from "../BooksItems";
import Helmet from 'react-helmet';

export class PublicBooks extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <BooksItems />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default PublicBooks