import React, { Component } from 'react';
import Slider from "../Slider";
import Headline from "../Headline";
import Home from "../Home";

export class PublicHome extends Component {
    render() {
        return (
            <div>
                <Slider />
                <Headline />
                <Home />
            </div>
        )
    }
}

export default PublicHome
