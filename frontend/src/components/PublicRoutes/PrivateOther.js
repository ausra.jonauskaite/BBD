import React, { Component } from 'react';
import OtherItemsPrivate from "../OtherItemsPrivate";
import Helmet from 'react-helmet';
import Navbar from '../Navbar';


export class PrivateOther extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <OtherItemsPrivate />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default PrivateOther