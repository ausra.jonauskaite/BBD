import React, { Component } from 'react'
import PublicGames from './PublicGames';
import PrivateGames from './PrivateGames';
import { connect } from "react-redux";
import PropTypes from "prop-types";



class GamesCategory extends Component {

    static propTypes = {
        auth: PropTypes.object.isRequired
    }

    render() {

        const { isAuthenticated, user } = this.props.auth;

        const authGames = (<PrivateGames />);
        const guestGames = (<PublicGames />);

        return (
            <div>
                { isAuthenticated ? authGames : guestGames}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(GamesCategory)
