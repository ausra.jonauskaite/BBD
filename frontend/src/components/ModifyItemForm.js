import React from 'react';
import './ItemForm.css';
import ModifyFormContent from './ModifyFormContent';

function ModifyItemForm() {
    return (
        <div className="form_container">
            <h2 className="form_headline">Redaguoti prekę</h2>
            <ModifyFormContent />
        </div>
    )
}

export default ModifyItemForm
