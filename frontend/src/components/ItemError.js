import React, { Component } from 'react';
import Navbar from './Navbar';
import ItemErrorCard from './ItemErrorCard';
import Helmet from 'react-helmet';

export class ItemError extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <ItemErrorCard />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default ItemError
