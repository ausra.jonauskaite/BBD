import React, { Component } from 'react'
import './SearchFilter.css';
import axios from 'axios';
import PropTypes from "prop-types";
import PrivateProduct from './PrivateProduct';
import './OtherItems.css';
import ItemsNotFound from './ItemsNotFound';
import { withRouter } from "react-router-dom";
import store from '../store';
import { tokenConfig } from '../actions/auth';


class PrivateItemsSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            filteredItems: [],
            sort: 'visos',
            quality: 'visos',
            sale: 'visos',
            categories: 'visos',
            loading: true,
            isEditing: false,
            searchTerm: '',
        };
        this.handleSortInput = this.handleSortInput.bind(this);
        this.handleStateInput = this.handleStateInput.bind(this);
        this.handleSaleInput = this.handleSaleInput.bind(this);
        this.handleCategoryInput = this.handleCategoryInput.bind(this);
    }

    componentDidMount() {
        const term = this.props.location.state.term;
        this.setState({ searchTerm: term });
        this.fetchSearch(term);
    }

    fetchSearch(term) {
        axios
            .get(`/api/products-logged?search=${term}`, tokenConfig(store.getState))
            .then(response => { this.setState({ items: response.data, loading: false }) })
            .catch(err => console.log(err));
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.location.state.term !== this.props.location.state.term) {
            this.setState({ searchTerm: this.props.location.state.term, isEditing: false })
            this.fetchSearch(this.props.location.state.term);
        }
    }

    handleSortInput(event) {
        this.setState({
            sort: event.target.value
        });
    }

    handleStateInput(event) {
        this.setState({
            quality: event.target.value
        });
    }

    handleSaleInput(event) {
        this.setState({
            sale: event.target.value
        });
    }

    handleCategoryInput(event) {
        this.setState({
            categories: event.target.value
        });
    }


    filterSort = (array) => {
        if (this.state.sort != 'visos') {
            return array.sort((a, b) => (this.state.sort === 'old') ? (a.creation_date > b.creation_date ? 1 : -1) : (a.creation_date < b.creation_date ? 1 : -1))
        } else {
            return array;
        }
    };

    filterQuality = (array) => {
        if (this.state.quality != 'visos') {
            return array.filter(a => a.state.indexOf(this.state.quality) >= 0)
        } else {
            return array;
        }
    };

    filterSale = (array) => {
        if (this.state.sale != 'visos') {
            return array.filter(a => JSON.stringify(a.for_sale) === this.state.sale)
        } else {
            return array;
        }
    };

    filterCategory = (array) => {
        if (this.state.categories != 'visos') {
            return array.filter(a => a.category.indexOf(this.state.categories) >= 0)
        } else {
            return array;
        }
    };


    filter = (e) => {
        e.preventDefault();
        let result = this.state.items;
        result = this.filterSort(result);
        result = this.filterCategory(result);
        result = this.filterQuality(result);
        result = this.filterSale(result);
        this.setState({ filteredItems: result, isEditing: true });
    }

    render() {

        const notFoundPage = (<ItemsNotFound />);

        const { loading } = this.state;

        if (loading) {
            return <div className="loading_display">Loading...</div>;
        }


        const Items = this.state.items.map((items, items_id) => {
            return (<PrivateProduct key={items_id}
                user_id={items.creator.id}
                item_id={items.id}
                userImage={items.creator.image}
                username={items.creator.username}
                itemImage={items.image}
                title={items.name}
                forSale={items.for_sale}
                creator_id={items.creator.id}
                inwishlist={items.in_wishlist}
            />
            )
        }
        );

        const sortedItems = this.state.filteredItems.map((items, items_id) => {
            return (<PrivateProduct key={items_id}
                user_id={items.creator.id}
                item_id={items.id}
                userImage={items.creator.image}
                username={items.creator.username}
                itemImage={items.image}
                title={items.name}
                forSale={items.for_sale}
                creator_id={items.creator.id}
                inwishlist={items.in_wishlist}
            />
            )
        }
        );

        return (
            <div>
                <div className="main_cont">
                    <div className="main_heading">
                        <div className="heading_box">
                            <div className="space_container">
                                <div className="title_container">
                                    <h2 className="search_title">{this.state.searchTerm}</h2>
                                </div>

                                <div className="filter_divider"></div>

                                <div className="filters_container">
                                    <div className="search_filter">
                                        <label className="filter_label">
                                            Rūšiuoti
                             </label>
                                        <div className="select_container">
                                            <select className="dropdown_w" value={this.state.sort} onChange={this.handleSortInput}>
                                                <option value="visos">Visos</option>
                                                <option value="new">Naujausios viršuje</option>
                                                <option value="old">Seniausios viršuje</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div className="search_quality">
                                        <label className="quality_label">
                                            Kategorija
                            </label>
                                        <div className="select_container">
                                            <select className="dropdown_w" value={this.state.categories} onChange={this.handleCategoryInput}>
                                                <option value="visos">Visos</option>
                                                <option value="Knygos">Knygos</option>
                                                <option value="Stalo žaidimai">Stalo žaidimai</option>
                                                <option value="Dviračiai / paspirtukai">Dviračiai / paspirtukai</option>
                                                <option value="Kitos prekės">Kitos prekės</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div className="search_quality">
                                        <label className="quality_label">
                                            Būklė
                            </label>
                                        <div className="select_container">
                                            <select className="dropdown_w" value={this.state.quality} onChange={this.handleStateInput}>
                                                <option value="visos">Visos</option>
                                                <option value="Nauja">Nauja</option>
                                                <option value="Labai gera">Labai gera</option>
                                                <option value="Gera">Gera</option>
                                                <option value="Patenkinama">Patenkinama</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div className="sale_filter">
                                        <label className="sale_label">
                                            Pardavimas
                            </label>
                                        <div className="select_container">
                                            <select className="dropdown_w" value={this.state.sale} onChange={this.handleSaleInput}>
                                                <option value="visos">Visos</option>
                                                <option value={true}>Taip</option>
                                                <option value={false}>Ne</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div className="search_f">
                                        <span className="material-icons md-24 filter_button" onClick={this.filter}>search</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="items_notFound">
                        <div className="notFound_title">
                            {sortedItems.length > 0 || Items.length > 0 ? '' : notFoundPage}
                        </div>
                    </div>
                </div>
                {/* ---------------------------------- ITEMS DISPLAY-------------------- */}
                <div className="otheritems_container">
                    <div className="resp_container">
                        <div className="otheritems_row">
                            {this.state.isEditing ? sortedItems : Items}
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}


export default withRouter(PrivateItemsSearch)
