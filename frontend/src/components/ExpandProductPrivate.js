import React, { Component } from 'react';
import './ExpandProduct.css';
import PrivateProductContent from './PrivateProductContent';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { getPrivateProduct } from '../actions/product';
import { getPrivateItems } from '../actions/privateitems';
import { postWishlist } from '../actions/wishlist';
import PrivateMap from './PrivateMap';
import './ItemMap.css';
import { Link, Redirect, Route } from "react-router-dom";
import PrivateProductForm from './PrivateProductForm';
import './PrivateProductContent.css';
import store from '../store';
import { tokenConfig } from '../actions/auth';
import axios from 'axios';
import './HeartBtn.css';

class ExpandProductPrivate extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            status: '',
            inwishlist: ''
        };
        this.toggleHeart = this.toggleHeart.bind(this);
    };

    static propTypes = {
        privateproduct: PropTypes.object.isRequired,
        creator: PropTypes.object.isRequired,
        getPrivateProduct: PropTypes.func.isRequired,
        postWishlist: PropTypes.func.isRequired,
    }

    componentDidMount() {
        const id = this.props.match.params.item_id;
        // console.log(id);
        this.props.getPrivateProduct(id);
        this.fetchPrivateItem(id);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.item_id !== this.props.match.params.item_id) {
            this.props.getPrivateProduct(this.props.match.params.item_id);
            this.fetchPrivateItem(this.props.match.params.item_id);
        }
    }

    fetchPrivateItem(id) {
        axios
            .get(`/api/products-logged/${id}`, tokenConfig(store.getState))
            .then(response => { this.setState({ inwishlist: response.data.in_wishlist, status: response.status, loading: false }) })
            .catch(err => { this.setState({ status: err.response.status, loading: false }) });
    }

    toggleHeart = (e) => {
        e.preventDefault();
        this.setState({ inwishlist: !this.state.inwishlist });
        this.props.postWishlist(this.props.match.params.item_id);
    }

    render() {

        // const heart_btnPrivate = { marginTop: '8px', marginRight: '15px', marginLeft: '10px', cursor: 'pointer' };
        // console.log(this.state.status);
        if (this.state.status === 404) {
            return <Redirect to="/404" />
        }

        if (this.state.loading) {
            return <div>Loading...</div>
        }

        return (
            <div>
                {this.props.sent.successStatus === 200 ? (<div className="alertMsg_container fade-in" ><div className="success_msg">
                    <p className="msg_cont">Žinutė išsiųsta sėkmingai</p>
                </div></div>) : ''}

                {this.props.notSent.status === 400 || this.props.notSent.status === 500 ? (<div className="alertMsg_container fade-in" ><div className="alert_Sentmsg">
                    <p className="msg_cont">žinutės išsiųsti nepavyko</p>
                </div></div>) : ''}

                <div className="product_container">
                    <div className="product_box">
                        <div>
                            <div className="content_container">
                                <div className="main_container">

                                    <div>
                                        <img src={this.props.privateproduct.image} alt="" className="product_image" />
                                    </div>

                                    <div className="content_cont">
                                        <div className="product_information">
                                            <div className="product_name">
                                                {this.props.privateproduct.for_sale ? <span className="material-icons md-28">monetization_on</span> : ""}
                                                <p>{this.props.privateproduct.name}</p>
                                            </div>
                                            <div className="content_divider"></div>

                                            <div className="info_display">
                                                <div className="txt_display">
                                                    <p>BŪKLĖ</p>
                                                    <p>PRADINĖ VERTĖ</p>
                                                    <p>KATEGORIJA</p>
                                                    <p>ADRESAS</p>
                                                    <p>ĮKĖLIMO DATA</p>
                                                    <p>KAINA</p>
                                                </div>

                                                <div className="txt_display">
                                                    <p>{this.props.privateproduct.state}</p>
                                                    <p>{this.props.privateproduct.primary_value} &#x20AC;</p>
                                                    <p>{this.props.privateproduct.category}</p>
                                                    <p>{this.props.privateproduct.location}</p>
                                                    <p>{this.props.privateproduct.creation_date && this.props.privateproduct.creation_date.substring(0, 10)}</p>
                                                    <p>{this.props.privateproduct.price} &#x20AC;</p>
                                                </div>
                                            </div>

                                            <div className="hearts-container">
                                                {this.props.user.id !== this.props.creator.id ? (
                                                    <div>
                                                        {this.state.inwishlist === false ? (<span className="material-icons" id="outline_hearts" onClick={this.toggleHeart}>favorite_border</span>) : (<span className="material-icons heart-position" onClick={this.toggleHeart} >favorite</span>)}
                                                    </div>
                                                ) : ('')}
                                            </div>
                                            <PrivateProductForm />
                                        </div>
                                    </div>
                                </div>

                                <div className="dbox_info">
                                    <p className="product_d">Aprašymas</p>
                                    <p className="product_dbox">{this.props.privateproduct.description}</p>
                                    <img src={this.props.creator.image} alt="" />
                                    <p className="usr_name">{this.props.creator.username}</p>
                                    <div className="content_div"></div>
                                </div>
                            </div>
                            <div className="map_container">
                                <div className="map_box">
                                    <div className="map_display2">
                                        <PrivateMap address={this.props.privateproduct.location} />
                                    </div>
                                    <div className="form_display">
                                        <Link to="/zemelapis">
                                            <div className="center_btn">
                                                <button className="search_btn3">Ieškoti daugiau prekių</button>
                                            </div>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    privateproduct: state.product.privateproduct,
    creator: state.product.privateproduct.creator,
    user: state.auth.user,
    sent: state.send,
    notSent: state.errors,
    errors: state.errors
});

export default withRouter(connect(mapStateToProps, { getPrivateProduct, getPrivateItems, postWishlist })(ExpandProductPrivate))

