import React from 'react';
import './Product.css';
import { Link } from "react-router-dom";


function Product({ username, userImage, itemImage, forSale, title, item_id }) {

    return (
        <div className="product">
            <div className="product_info">

                <div className="user_info">
                    <img src={userImage} alt="" />
                    <p>{username}</p>
                </div>

                <Link to={"/preke/" + item_id} className="product_info">
                    <img src={itemImage} alt="" />
                </Link>

                <div className="product_title">
                    {forSale ?
                        <span className="material-icons">paid</span>
                        : ""
                    }
                    <p>{title}</p>
                </div>

            </div>
        </div>
    )
}


export default Product
