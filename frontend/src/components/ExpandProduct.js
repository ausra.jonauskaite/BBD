import React, { Component } from 'react';
import './ExpandProduct.css';
// import ProductContent from './ProductContent';
// import JSONDATA from './MOCK_DATA_CONTENT.json';
import { connect } from 'react-redux';
import { Redirect, withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { getProduct } from '../actions/product';
import PublicMap from './PublicMap';
import './ItemMap.css';
import axios from 'axios';
import './ProductContent.css';

class ExpandProduct extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            status: ''
        };
    };

    static propTypes = {
        product: PropTypes.object.isRequired,
        creator: PropTypes.object.isRequired,
        getProduct: PropTypes.func.isRequired
    }

    componentDidMount() {
        const id = this.props.match.params.item_id;
        // console.log(id);
        this.props.getProduct(id);
        this.fetchItem(id);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.item_id !== this.props.match.params.item_id) {
            this.props.getProduct(this.props.match.params.item_id);
            this.fetchItem(this.props.match.params.item_id);
        }
    }

    fetchItem(id) {
        axios
            .get(`/api/products/${id}`)
            .then(response => { this.setState({ status: response.status, loading: false }) })
            .catch(err => { this.setState({ status: err.response.status, loading: false }) });
    }

    // handleClick(e) {
    //     e.preventDefault();
    //     return <Redirect to="/prisijungimas" />
    // }

    render() {

        if (this.state.status === 404) {
            return <Redirect to="/404" />
        }

        if (this.state.loading) {
            return <div>Loading...</div>
        }

        return (
            <div className="product_container">
                <div className="product_box">
                    <div>
                        <div className="content_container">
                            <div className="main_container">

                                <div>
                                    <img src={this.props.product.image} alt="" className="product_image" />
                                </div>

                                <div className="content_cont">
                                    <div className="product_information">
                                        <div className="product_name">
                                            {this.props.product.for_sale ? <span className="material-icons md-28">monetization_on</span> : ""}
                                            <p>{this.props.product.name}</p>
                                        </div>
                                        <div className="content_divider"></div>

                                        <div className="info_display">
                                            <div className="txt_display">
                                                <p>BŪKLĖ</p>
                                                <p>PRADINĖ VERTĖ</p>
                                                <p>KATEGORIJA</p>
                                                <p>ADRESAS</p>
                                                <p>ĮKĖLIMO DATA</p>
                                                <p>KAINA</p>
                                            </div>

                                            <div className="txt_display">
                                                <p>{this.props.product.state}</p>
                                                <p>{this.props.product.primary_value} &#x20AC;</p>
                                                <p>{this.props.product.category}</p>
                                                <p>{this.props.product.location}</p>
                                                <p>{this.props.product.creation_date && this.props.product.creation_date.substring(0, 10)}</p>
                                                <p>{this.props.product.price} &#x20AC;</p>
                                            </div>
                                        </div>
                                        <Link to="/prisijungimas"> <button className="interest_Btn">Domina</button></Link>
                                    </div>
                                </div>
                            </div>

                            <div className="dbox_info">
                                <p className="product_D">Aprašymas</p>
                                <p className="product_desc">{this.props.product.description}</p>

                                <img src={this.props.creator.image} alt="" />
                                <p className="usr_name">{this.props.creator.username}</p>
                                <div className="content_div"></div>
                            </div >
                        </div>

                        <div className="map_container">
                            <div className="map_box">
                                <div className="map_display2">
                                    <PublicMap address={this.props.product.location} />
                                </div>
                                <div className="form_display">
                                    <Link to="/prisijungimas">
                                        <div className="center_btn">
                                            <button className="search_btn3">Ieškoti daugiau prekių</button>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    product: state.product.product,
    creator: state.product.product.creator
});

export default connect(mapStateToProps, { getProduct })(withRouter(ExpandProduct));

