import React, { Component } from 'react';
import './Maps.css';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import store from '../store';
import { tokenConfig } from '../actions/auth';
import { getUserData, getResultsData, getRadiusData } from '../actions/maps';
import axios from 'axios';
import Geocode from "react-geocode";
import MapsHooks from './MapsHooks';
import './OtherItems.css';
import './SearchFilter.css';

class Maps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      filtered_items: [],
      errorMessage: '',
      loading: true,
      userCoordinates: '',
      addr_array: [],
      quality: 'visos',
      term: '',
      sale: 'visos',
      value: 'visos'
    };
    this.handleCategoryInput = this.handleCategoryInput.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleSearchInput = this.handleSearchInput.bind(this);
    this.handleStateInput = this.handleStateInput.bind(this);
    this.handleSaleInput = this.handleSaleInput.bind(this);
  }

  state = {
    number: ""
  }

  static propTypes = {
    getUserData: PropTypes.func.isRequired,
    getResultsData: PropTypes.func.isRequired,
    getRadiusData: PropTypes.func.isRequired
  }

  componentDidMount() {
    this.fetchItems();
    Geocode.setApiKey("AIzaSyA3uCc5jzfhQ7JzCvpAf-wWljdJzICn0Y4");
    this.getUserCoordinates();
  }

  getUserCoordinates = async () => {
    let location = await Geocode.fromAddress(this.props.user.address);
    location = location.results[0].geometry.location;
    this.setState({ userCoordinates: location });
    this.props.getUserData(location);
  }

  getItemsCoordinates = async (products) => {
    // let result = products.map(a => a.location);
    let data = [];
    for (var i = 0; i < products.length; i++) {
      // if (google.maps.GeocoderStatus.ZERO_RESULTS === 'ZERO_RESULTS') {
      let location = await Geocode.fromAddress(products[i].location);
      location = location.results[0].geometry.location;
      let item = products[i];
      let newItem = Object.assign(item, location);
      data.push(newItem);
      // continue;
      // }
    }

    // console.log(data);
    this.setState({ addr_array: data });
    this.props.getResultsData(data);
  }

  fetchItems() {
    axios
      .get('/api/products-logged', tokenConfig(store.getState))
      .then(response => { this.setState({ items: response.data, loading: false }) })
      .catch(err => console.log(err));
  }


  handleCategoryInput(event) {
    this.setState({
      value: event.target.value
    });
  }

  handleStateInput(event) {
    this.setState({
      quality: event.target.value
    });
  }

  handleSaleInput(event) {
    this.setState({
      sale: event.target.value
    });
  }

  handleSearchInput(event) {
    this.setState({
      term: event.target.value
    });
  }

  handleInput = event => {
    const number = event.target.value;
    if (!number || number.match(/^\d{1,}(\.\d{0,2})?$/)) {
      this.setState({ number }, () => {
        this.validateRadius();
      });
    }
    // this.setState({
    //   number: event.target.value
    // });
  };

  validateRadius = () => {
    const { number } = this.state;

    if (number.length == 0 || number > 1000) {
      this.setState({ numberError: 'Neteisingai nurodyta spindulio vertė' });
    } else {
      this.setState({ numberError: '' });
    }
  };

  // filters

  filterQuality = (array) => {
    if (this.state.quality !== 'visos') {
      return array.filter(a => a.state.indexOf(this.state.quality) >= 0)
    } else {
      return array;
    }
  };

  filterSale = (array) => {
    if (this.state.sale !== 'visos') {
      return array.filter(a => JSON.stringify(a.for_sale) === this.state.sale)
    } else {
      return array;
    }
  };

  filterCategory = (array) => {
    if (this.state.value !== 'visos') {
      return array.filter(a => a.category.indexOf(this.state.value) >= 0)
    } else {
      return array;
    }
  };

  titleFormatter(txt) {
    let newValue = txt.toLowerCase()
      .replaceAll("ą", "a")
      .replaceAll("č", "c")
      .replaceAll("ę", "e")
      .replaceAll("ė", "e")
      .replaceAll("į", "i")
      .replaceAll("š", "s")
      .replaceAll("ų", "u")
      .replaceAll("ū", "u")
      .replaceAll("ž", "z")
      .replaceAll(/[/=-_)(*£"¬`;|~'"?.,+!@#$%^&*]+/g, "")

    return newValue
  }

  filterTerm = (array) => {
    if (this.state.term !== '' || this.state.term !== null || this.state.term !== undefined) {
      const title = this.titleFormatter(this.state.term);
      return array.filter(a => this.titleFormatter(a.name).toLowerCase().indexOf(title) >= 0)
    } else {
      console.log(array);
      return array;
    }
  };


  filter = (e) => {
    e.preventDefault();
    const number = this.state.number;
    this.props.getRadiusData(number);

    let result = this.state.items;
    result = this.filterTerm(result);
    result = this.filterCategory(result);
    result = this.filterQuality(result);
    result = this.filterSale(result);
    this.setState({ filtered_items: result });
    this.getItemsCoordinates(result);
  };


  render() {

    const { userCoordinates, addr_array } = this.state;

    return (
      <div className="map_container2">
        <div className="map_box2">
          <div className="map_disp">
            <MapsHooks />
          </div>

          <form className="form_display2">
            <div className="section_line2"></div>


            {/* word search */}
            <label className="label-display">Paieškos frazė</label>
            <input type="text" placeholder="Ieškoti" className="input-search-container" onChange={this.handleSearchInput} value={this.state.term} />
            <div className="filter-containers">
              {/* category dropdown */}
              <div className="filters_container">

                {/* category dropdown */}
                <div className="search_quality">
                  <label className="label-display">
                    Kategorija
                            </label>
                  <div className="select-container">
                    <select className="dropdown_w" onChange={this.handleCategoryInput} value={this.state.value}>
                      <option value="visos">Visos</option>
                      <option value="Knygos">Knygos</option>
                      <option value="Stalo žaidimai">Stalo žaidimai</option>
                      <option value="Dviračiai / paspirtukai">Dviračiai / paspirtukai</option>
                      <option value="Kitos prekės">Kitos prekės</option>
                    </select>
                  </div>
                </div>

                <div className="search_quality">
                  <label className="label-display">
                    Būklė
                            </label>
                  <div className="select-container">
                    <select className="dropdown_w" value={this.state.quality} onChange={this.handleStateInput}>
                      <option value="visos">Visos</option>
                      <option value="Nauja">Nauja</option>
                      <option value="Labai gera">Labai gera</option>
                      <option value="Gera">Gera</option>
                      <option value="Patenkinama">Patenkinama</option>
                    </select>
                  </div>
                </div>

                <div className="sale_filter">
                  <label className="label-display">
                    Pardavimas
                            </label>
                  <div className="select-container">
                    <select className="dropdown_w" value={this.state.sale} onChange={this.handleSaleInput}>
                      <option value="visos">Visos</option>
                      <option value={true}>Taip</option>
                      <option value={false}>Ne</option>
                    </select>
                  </div>
                </div>

                <div className="sale_filter">
                  {/* search radius */}
                  <label className="label-display search-label">Paieškos spindulis (km)</label>
                  <input type="text" placeholder="0-1000" className="search-radius" onChange={this.handleInput} value={this.state.number} />
                  {/* select choice ends here */}
                </div>
              </div>
            </div>
            <button className="search_btn2" onClick={this.filter}>Ieškoti prekių</button>

          </form>

        </div>
      </div >
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user
});

export default connect(mapStateToProps, { getUserData, getResultsData, getRadiusData })(Maps)
