import React, { Component } from 'react';
import './PrivateProductForm.css';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import FormModal from './FormModal';
import Form from "react-bootstrap/Form";
import { getUserItems } from '../actions/userinfo';
import { sendForm } from '../actions/send';
// import HeartBtn from './HeartBtn';
// import AdditionalHeart from './AdditionalHeart';
import HeartBtn2 from './HeartBtn2';

export class PrivateProductForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // selectedItem: "Pasirinkti prekę",
        };
        this.handleSelection = this.handleSelection.bind(this);
        this.handleComment = this.handleComment.bind(this);
    };

    state = {
        isOpen: false,
        selectedItem: "Pasirinkite prekę",
        checkedEmail: false,
        checkedPhone: false,
        comment: '',
        checkboxError: '',
        itemError: '',
        commentError: '',
        product_id: '',
        blankError: true,
        updateSuccess: false,
        dboxError: ''
    }

    toggleModal = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    toggleEmail = () => {
        this.setState({
            checkedEmail: !this.state.checkedEmail
        });
    }

    togglePhone = () => {
        this.setState({
            checkedPhone: !this.state.checkedPhone
        });
    }

    handleSelection(e) {
        this.setState({ selectedItem: e.target.value });
    }

    componentDidMount() {
        this.props.getUserItems();
    }

    // Handle Comment
    handleComment = event => {
        this.setState({ comment: event.target.value }, () => {
            this.validateComment();
        });
    }


    validateComment = () => {
        const { comment } = this.state;

        if (comment.length == 0) {
            this.setState({ commentError: 'Komentaras negali būti tuščias' });
        } else {
            this.setState({ commentError: '' });
        }
    }


    // validate psw form
    validateForm = () => {

        let checkboxError = "";
        // let itemError = "";
        let commentError = "";

        if (this.state.isOpen) {

            if ((this.state.checkedEmail === false && this.state.checkedPhone === false) || (this.state.checkedEmail === undefined && this.state.checkedPhone === undefined) || (this.state.checkedEmail === null && this.state.checkedPhone === null)) {
                checkboxError = 'Pažymėkite bent 1 pasirinkimą';
            }

            if (checkboxError) {
                this.setState({ checkboxError: checkboxError });
                return false;
            }

            // comment validation
            if (this.state.comment === undefined || this.state.comment === null || this.state.comment.length == 0) {
                commentError = 'Komentaras negali būti tuščias';
            }

            if (commentError) {
                return false;
            }

            return true;
        }
    };

    // Form validation
    // Selection 

    onSubmit = (e) => {
        e.preventDefault();
        const isValid = this.validateForm();

        const { selectedItem, checkedEmail, checkedPhone, comment } = this.state;
        const product_id = this.props.product.id;

        if (isValid) {
            if (checkedEmail === undefined) {
                const sendEmail = false;
                this.props.sendForm(product_id, selectedItem, comment, sendEmail, checkedPhone);
                this.toggleModal(false);
            }
            if (checkedPhone === undefined) {
                const sendPhone = false;
                this.props.sendForm(product_id, selectedItem, comment, checkedEmail, sendPhone);
                this.toggleModal(false);
            }
            if (checkedEmail === true && checkedPhone === true) {
                this.props.sendForm(product_id, selectedItem, comment, checkedEmail, checkedPhone);
                this.toggleModal(false);
            }
        } else {
            this.setState({ blankError: true });
            setTimeout(function () { this.setState({ blankError: false }) }.bind(this), 4000);
        }
    };

    static propTypes = {
        getUserItems: PropTypes.func.isRequired,
        sendForm: PropTypes.func.isRequired,
        product: PropTypes.func.isRequired
    }

    render() {

        const options = this.props.items.map(v => ({ label: v.name, value: v.id, img: v.image }));
        const heart_btnPrivate = { marginTop: '8px', marginRight: '15px', marginLeft: '10px', cursor: 'pointer' };
        // console.log(this.state.selectedItem);
        // console.log(this.state.checkedEmail);
        // console.log(this.state.checkedPhone);
        // console.log(this.state.comment);

        return (
            <div>
                <div className="buttons_container">
                    {/* <div style={heart_btnPrivate}>
                        {this.props.user.id !== this.props.creator.id ? (<HeartBtn2 />) : ('')}
                    </div> */}
                    {this.props.user.id !== this.props.creator.id ? (<button className="interest_btn_form" onClick={this.toggleModal}>Domina</button>) : ''}
                </div>

                {/* add modal form below */}
                <Form onSubmit={this.onSubmit}>
                    <FormModal show={this.state.isOpen} className="form_modal">

                        <div className="error_container">
                            {this.state.blankError ? (<div className="alertMsg-container fade-in" ><div className="alert-msg">
                                <p className="msg-cont">Klaida! Neteisingai nurodyti duomenys</p>
                            </div></div>) : ''}

                            <div>{this.state.blankError}</div>

                        </div>

                        <div className="selection_positioning">
                            <p className="info_selection">El. paštas</p>
                            <input className="popup_checkbox1" type="checkbox" value={this.state.checkedEmail} onClick={this.toggleEmail} />
                        </div>

                        <div className="selection_positioning">
                            <p className="info_selection">Telefono numeris</p>
                            <input className="popup_checkbox2" type="checkbox" value={this.state.checkedPhone} onClick={this.togglePhone} />
                        </div>
                        <div className='input_errors'>{this.state.checkboxError}</div>

                        <div className="content_divider"></div>

                        {/* select item */}
                        <label className="label_title">
                            Pasirinkite prekę mainams
                        </label>
                        <select className="exchange_item" size="5" value={this.state.selectedItem} onChange={this.handleSelection}>
                            {/* <option value="Pasirinkite prekę">Pasirinkite prekę</option> */}
                            {options.map(option => (
                                <option value={option.value} >
                                    {option.label}
                                </option>
                            ))}
                        </select>
                        {/* <div className='input_errors'>{this.state.itemError}</div> */}

                        {/* comment */}
                        <label className="label_title">
                            Komentaras
                        </label>
                        <textarea rows="10" className="comment_box" maxLength="900" value={this.state.comment} onChange={this.handleComment} />
                        <div className='input_errors'>{this.state.commentError}</div>
                        <br />
                        <div className="btn_pos">
                            <button className="cancel_Btn" onClick={this.toggleModal} >Atšaukti</button>
                            <button className="valid_Btn" onSubmit={this.onSubmit} >Siųsti</button>
                        </div>
                    </FormModal>
                </Form>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.auth.user,
    creator: state.product.privateproduct.creator,
    items: state.userinfo.items,
    product: state.product.privateproduct,
    send: state.send
});

export default connect(mapStateToProps, { getUserItems, sendForm })(PrivateProductForm)
