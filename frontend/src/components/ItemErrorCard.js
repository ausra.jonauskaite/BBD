import React, { Component } from 'react';
import './ItemCard.css';
import { Redirect } from "react-router-dom";

export class ItemErrorCard extends Component {
    state = {
        redirect: false
    }

    componentDidMount() {
        this.id = setTimeout(() => this.setState({ redirect: true }), 3000)
    }

    render() {
        return (
            <div>
                {this.state.redirect ? (<Redirect to="/ikelti_preke" />) : (<div className="main-container">
                    <div className="card-container">
                        <div className="card-item">
                            <img src="/static/error.png" alt="" />
                            <div className="title-container">
                                <p>Įvyko klaida!</p>
                            </div>
                            <p className="card-info">Bandykite dar kartą.</p>
                        </div>
                    </div>
                </div>)}
            </div>
        )
    }
}

export default ItemErrorCard
