import React, { Component } from 'react';
import './Notifications.css';
import store from '../store';
import { tokenConfig } from '../actions/auth';
import './ScrollToTop.css';
import axios from 'axios';
import { markAllRead, getMessages, markOneRead } from '../actions/auth';
import { connect } from "react-redux";
import PropTypes from "prop-types";

class Notifications extends Component {
    constructor(props) {
        super(props);
        this.state = {
            is_visible: false,
            isClicked: false
        };
    }

    static propTypes = {
        markAllRead: PropTypes.func.isRequired,
        getMessages: PropTypes.func.isRequired
    }

    componentDidMount() {
        const { isClicked } = this.state;
        this.props.getMessages();
        this.setState({ isClicked: !isClicked });
        let scrollComponent = this;
        document.addEventListener("scroll", function (e) {
            scrollComponent.toggleVisibility();
        });
    }

    componentDidUpdate(prevProps, prevState) {
        const { isClicked } = this.state;
        if (prevState.isClicked !== isClicked) {
            this.props.getMessages();
            // console.log(prevState);
        }
    }

    // formatter(message) {
    //     let str = message;
    //     let array = str.split("\n");
    //     let result;

    //     for (let i = 0; i < array.length; i++) {
    //         result += array[i] + "\r\n";
    //         console.log(result);
    //     }

    //     return result.replace('undefined', '');

    // }

    toggleVisibility() {
        if (window.pageYOffset > 300) {
            this.setState({
                is_visible: true
            });
        } else {
            this.setState({
                is_visible: false
            });
        }
    }

    scrollToTopBtn() {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    };

    markAll = (e) => {
        e.preventDefault();
        const { isClicked } = this.state;
        const user_id = this.props.user.id;
        this.props.markAllRead(user_id);
        this.setState({ isClicked: !isClicked });
    }

    markAsRead(id) {
        // e.preventDefault();
        const { isClicked } = this.state;
        this.props.markOneRead(id);
        this.setState({ isClicked: !isClicked });
    }

    render() {

        const { is_visible } = this.state;
        // console.log(this.state.isClicked);

        return (
            <div className="notification_container">
                <h2 className="notification_headline">Pranešimai</h2>
                <div className="allRead_btnPosition">
                    <div>
                        <button className="allRead_btn" onClick={this.markAll}>PAŽYMĖTI VISUS</button>
                    </div>
                </div>


                {this.props.notifications.map((val, key) => {
                    return (
                        <div className={val.is_read ? 'content_box' : 'content_box_unread'}>
                            <div className="message_container">
                                <div className="read_btn_pos">
                                    {val.is_read ? '' : (<button className="mark_read" onClick={() => this.markAsRead(val.id)}>Pažymėti kaip skaitytą</button>)}
                                </div>
                                <div className="user_content">
                                    <div className="user_data">
                                        <img src={val.sender.image} alt="" />
                                        <p>{val.sender.username}</p>
                                    </div>

                                    <p className="user_message">{val.message}</p>
                                </div>
                                <div className="time_display">{val.timestamp.substring(0, 10)}</div>
                                <div className="bottom_line"></div>
                            </div>

                        </div>
                    );
                })}

                <div className="scroll-to-top">
                    {is_visible && (
                        <div onClick={() => this.scrollToTopBtn()}>
                            <span id="scroll-icon" className="material-icons">keyboard_arrow_up</span>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    notifications: state.auth.notifications,
    user: state.auth.user,
    status: state.auth
});

export default connect(mapStateToProps, { markAllRead, getMessages, markOneRead })(Notifications)
