// import React, { Component } from 'react';
// import { connect } from "react-redux";
// import PropTypes from "prop-types";
// import './HeartBtn.css';
// import _ from 'lodash';
// import ToggleBtn2 from './ToggleBtn2';
// import { postWishlist } from '../actions/wishlist';
// import { withRouter } from 'react-router-dom';


// class HeartBtn2 extends Component {

//     constructor(props) {
//         super(props);
//         this.state = {
//             wishes: [
//                 { id: this.props.match.params.item_id, have: this.props.location.wishlist }
//             ]
//         };
//     }

//     // componentDidMount() {
//     //     const id = this.props.match.params.item_id;
//     //     let routeState
//     //     if (this.props.location.wishlist) {
//     //         localStorage.setItem('routeState', JSON.stringify(this.props.location.wishlist))
//     //         routeState = this.props.location.wishlist
//     //     } else {
//     //         routeState = localStorage.getItem('routeState')
//     //         if (routeState) routeState = JSON.parse(routeState)
//     //     }

//     //     if (routeState) {
//     //         use routeState ahead
//     //         console.log(routeState);
//     //         this.setState(prevState => {
//     //             let wishes = Object.assign({}, prevState.wishes);  
//     //             wishes.have = routeState;                                     
//     //             return { wishes };                               
//     //           });
//     //     } else {
//     //         Prompt no data.
//     //         console.log('Something went wrong');
//     //     }
//     // }

//     toggleActive(wishId) {
//         // console.log(wishId);
//         let newState = Object.assign({}, this.state);
//         let wish = _.find(newState.wishes, { id: wishId });
//         // console.log(wish);
//         wish.have = !wish.have;
//         this.setState(newState);
//         this.props.postWishlist(wishId);
//     }


//     render() {
//         console.log(this.state.wishes);

//         return (
//             <ToggleBtn2 wishes={this.state.wishes} toggleActive={(wishId) => this.toggleActive(wishId)} />
//         );
//     }
// }

// const mapStateToProps = state => ({
//     product: state.product.privateproduct
// });


// export default withRouter(connect(mapStateToProps, { postWishlist })(HeartBtn2))
