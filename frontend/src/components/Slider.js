import React from 'react';
import './Slider.css';
// import { Slide } from 'react-slideshow-image';
// import 'react-slideshow-image/dist/styles.css'

const images = ["/static/Poster_v1.jpg", "/static/Poster_2.jpg", "/static/Poster_3.jpg", "/static/Poster_4.jpg", "/static/Poster_5.jpg", "/static/Poster_6.jpg"];
const delay = 5000;

function Slider() {
    const [index, setIndex] = React.useState(0);
    const timeoutRef = React.useRef(null);

    function resetTimeout() {
        if (timeoutRef.current) {
            clearTimeout(timeoutRef.current);
        }
    }

    React.useEffect(() => {
        resetTimeout();
        timeoutRef.current = setTimeout(
            () =>
                setIndex((prevIndex) =>
                    prevIndex === images.length - 1 ? 0 : prevIndex + 1
                ),
            delay
        );

        return () => {
            resetTimeout();
        };
    }, [index]);

    return (
        <div className="slideshow">
            <div
                className="slideshowSlider"
                style={{ transform: `translate3d(${-index * 100}%, 0, 0)` }}
            >
                {images.map((each, index) => <img className="img_container" key={index} style={{ width: "100%" }} src={each} />)}
            </div>

            {/* <div className="slideshowDots">
                {images.map((_, idx) => (
                    <div
                        key={idx}
                        className={`slideshowDot${index === idx ? " active" : ""}`}
                        onClick={() => {
                            setIndex(idx);
                        }}
                    ></div>
                ))}
            </div> */}
        </div>
    );

}

export default Slider
