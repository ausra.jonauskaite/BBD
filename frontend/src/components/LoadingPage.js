import React, { Component } from 'react'
import './LoadingPage.css';

export class LoadingPage extends Component {
    render() {
        return (
            <div>
                <p className="loading-page">PLEASE WAIT. IT IS LOADING...</p>
            </div>
        )
    }
}

export default LoadingPage
