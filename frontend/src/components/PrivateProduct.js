import React from 'react';
import './Product.css';
import { Link } from "react-router-dom";
import HeartBtn from './HeartBtn';
import { useDispatch, useSelector } from "react-redux";


function PrivateProduct({ username, userImage, itemImage, forSale, title, item_id, creator_id, inwishlist }) {

    const user = useSelector(state => state.auth.user);
    // console.log('random text');

    return (
        <div className="product">
            <div className="product_info">

                <div className="user_info">
                    <img src={userImage} alt="" />
                    <p>{username}</p>
                </div>

                <Link to={{ pathname: "/preke_/" + item_id, wishlist: inwishlist }} className="product_info">
                    <img src={itemImage} alt="" />
                </Link>

                <div className="product_title">
                    {forSale ?
                        <span className="material-icons">paid</span>
                        : ""
                    }
                    <p>{title}</p>
                </div>

                <div className="heartBtn_display">
                    {user.id !== creator_id ? (<HeartBtn itemId={item_id} heart={inwishlist} />) : ('')}
                </div>

            </div>
        </div>
    )
}


export default PrivateProduct
