import React, { Component } from 'react'
import './Home.css';
import Product from './Product';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getItems } from '../actions/items';
import ReactDOM from 'react-dom';
import './ScrollToTop.css';
// import InfiniteScroll from "react-infinite-scroll-component";

class HomeItemsPublic extends Component {

    constructor(props) {
        super(props);
        this.state = {
            is_visible: false
        };
    }

    static propTypes = {
        items: PropTypes.object.isRequired,
        getItems: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.props.getItems();
        var scrollComponent = this;
        document.addEventListener("scroll", function (e) {
            scrollComponent.toggleVisibility();
        });
    }

    toggleVisibility() {
        if (window.pageYOffset > 300) {
            this.setState({
                is_visible: true
            });
        } else {
            this.setState({
                is_visible: false
            });
        }
    }

    scrollToTopBtn() {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    }

    render() {

        const { is_visible } = this.state;

        return (
            <div>
                <div className="home">
                    {/*Product*/}
                    <div className="home_row">
                        {this.props.items.map(items => (
                            <Product key={items.id}
                                item_id={items.id}
                                userImage={items.creator.image}
                                username={items.creator.username}
                                itemImage={items.image}
                                title={items.name}
                                forSale={items.for_sale}
                            />
                        ))}
                    </div>
                    <div className="scroll-to-top">
                        {is_visible && (
                            <div onClick={() => this.scrollToTopBtn()}>
                                <span id="scroll-icon" className="material-icons">keyboard_arrow_up</span>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    items: state.items.items
});

export default connect(mapStateToProps, { getItems })(HomeItemsPublic)