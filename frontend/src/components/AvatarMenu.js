import React, { useState } from 'react'
import './AvatarMenu.css';
import Menu from "./Menu";
import ReactDOM from "react-dom";
import { Link } from "react-router-dom";
import { logout } from '../actions/auth';
import { connect } from "react-redux";
import PropTypes from "prop-types";


function AvatarMenu({ user_img }) {

    const [isMenuOpen, toggleMenu] = useState(false);

    return (
        <div>

            <div className="avatar_menu" onClick={() => toggleMenu(!isMenuOpen)}>
                <img src={user_img} alt="" />
            </div>
            <div >
                <Menu className="menu_styles" isOpen={isMenuOpen} toggle={toggleMenu}>
                    <p className="menu_headline">PASKYRA</p>
                    <Link to="/profilis" className="menu_section"> <p className="menu_section" onClick={() => toggleMenu(false)}>Mano paskyra</p> </Link>
                    <Link to="/redaguoti_skelbimus" className="menu_section"><p className="menu_section" onClick={() => toggleMenu(false)}>Redaguoti skelbimus</p></Link>
                    <Link to="/pazymetos_prekes" className="menu_section"><p className="menu_section" onClick={() => toggleMenu(false)}>Pažymėtos prekės</p></Link>
                </Menu>
            </div>
        </div>
    )
}

export default AvatarMenu;
