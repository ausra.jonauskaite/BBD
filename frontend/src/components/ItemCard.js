import React, { Component } from 'react';
import './ItemCard.css';
import { Redirect } from "react-router-dom";

export class ItemCard extends Component {
    state = {
        redirect: false
    }

    componentDidMount() {
        this.id = setTimeout(() => this.setState({ redirect: true }), 3000)
    }

    render() {
        return (
            <div>
                {this.state.redirect ? (<Redirect to="/" />) : (<div className="main-container">
                    <div className="card-container">
                        <div className="card-item">
                            <img src="/static/success.png" alt="" />
                            <div className="title-container">
                                <p>Skelbimas įkeltas!</p>
                            </div>
                            <p className="card-info">Netrukus nukelsime į pagrindinį puslapį...</p>
                        </div>
                    </div>
                </div>)}
            </div>
        )
    }
}

export default ItemCard
