import React, { Component } from 'react'
import './SearchFilter.css';
import axios from 'axios';
import PropTypes from "prop-types";
import Product from './Product';
import './OtherItems.css';
import './ScrollToTop.css';
export class BooksItems extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            filteredItems: [],
            sort: 'visos',
            quality: 'visos',
            sale: 'visos',
            loading: true,
            isEditing: false,
            is_visible: false
        };
        this.handleSortInput = this.handleSortInput.bind(this);
        this.handleStateInput = this.handleStateInput.bind(this);
        this.handleSaleInput = this.handleSaleInput.bind(this);
    }

    componentDidMount() {
        this.fetchBooks();
        let scrollComponent = this;
        document.addEventListener("scroll", function (e) {
            scrollComponent.toggleVisibility();
        });
    }

    fetchBooks() {
        axios
            .get('/api/products?category=Knygos')
            .then(response => { this.setState({ items: response.data, loading: false }) })
            .catch(err => console.log(err));
    }

    handleSortInput(event) {
        this.setState({
            sort: event.target.value
        });
    }

    handleStateInput(event) {
        this.setState({
            quality: event.target.value
        });
    }

    handleSaleInput(event) {
        this.setState({
            sale: event.target.value
        });
    }


    filterSort = (array) => {
        if (this.state.sort != 'visos') {
            return array.sort((a, b) => (this.state.sort === 'old') ? (a.creation_date > b.creation_date ? 1 : -1) : (a.creation_date < b.creation_date ? 1 : -1))
        } else {
            return array;
        }
    };

    filterQuality = (array) => {
        if (this.state.quality != 'visos') {
            return array.filter(a => a.state.indexOf(this.state.quality) >= 0)
        } else {
            return array;
        }
    };

    filterSale = (array) => {
        if (this.state.sale != 'visos') {
            return array.filter(a => JSON.stringify(a.for_sale) === this.state.sale)
        } else {
            return array;
        }
    };


    filter = (e) => {
        e.preventDefault();
        let result = this.state.items;
        result = this.filterSort(result);
        result = this.filterQuality(result);
        result = this.filterSale(result);
        this.setState({ filteredItems: result, isEditing: true });
    };

    toggleVisibility() {
        if (window.pageYOffset > 300) {
            this.setState({
                is_visible: true
            });
        } else {
            this.setState({
                is_visible: false
            });
        }
    }

    scrollToTopBtn() {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    };


    render() {

        const { loading } = this.state;
        const { is_visible } = this.state;

        if (loading) {
            return <div className="loading_display">Loading...</div>;
        }

        const categoryName = "Knygos";

        const Items = this.state.items.map((items, items_id) => {
            if (items.category === categoryName) {
                return (<Product key={items_id}
                    user_id={items.creator.id}
                    item_id={items.id}
                    userImage={items.creator.image}
                    username={items.creator.username}
                    itemImage={items.image}
                    title={items.name}
                    forSale={items.for_sale}
                />
                )
            }
        }
        );

        const sortedItems = this.state.filteredItems.map((items, items_id) => {
            if (items.category === categoryName) {
                return (<Product key={items_id}
                    user_id={items.creator.id}
                    item_id={items.id}
                    userImage={items.creator.image}
                    username={items.creator.username}
                    itemImage={items.image}
                    title={items.name}
                    forSale={items.for_sale}
                />
                )
            }
        }
        );

        return (
            <div>
                <div className="main_cont">
                    <div className="main_heading">
                        <div className="heading_box">
                            <div className="space_container">
                                <div className="title_container">
                                    <h2 className="search_title">Knygos</h2>
                                </div>

                                <div className="filter_divider"></div>

                                <div className="filters_container">
                                    <div className="search_filter">
                                        <label className="filter_label">
                                            Rūšiuoti
                             </label>
                                        <div className="select_container">
                                            <select className="dropdown_w" value={this.state.sort} onChange={this.handleSortInput}>
                                                <option value="visos">Visos</option>
                                                <option value="new">Naujausios viršuje</option>
                                                <option value="old">Seniausios viršuje</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div className="search_quality">
                                        <label className="quality_label">
                                            Būklė
                            </label>
                                        <div className="select_container">
                                            <select className="dropdown_w" value={this.state.quality} onChange={this.handleStateInput}>
                                                <option value="visos">Visos</option>
                                                <option value="Nauja">Nauja</option>
                                                <option value="Labai gera">Labai gera</option>
                                                <option value="Gera">Gera</option>
                                                <option value="Patenkinama">Patenkinama</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div className="sale_filter">
                                        <label className="sale_label">
                                            Pardavimas
                            </label>
                                        <div className="select_container">
                                            <select className="dropdown_w" value={this.state.sale} onChange={this.handleSaleInput}>
                                                <option value="visos">Visos</option>
                                                <option value={true}>Taip</option>
                                                <option value={false}>Ne</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div className="search_f">
                                        <span className="material-icons md-24 filter_button" onClick={this.filter}>search</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* ---------------------------------- ITEMS DISPLAY-------------------- */}
                <div className="otheritems_container">
                    <div className="resp_container">
                        <div className="otheritems_row">
                            {this.state.isEditing ? sortedItems : Items}
                        </div>
                        <div className="scroll-to-top">
                            {is_visible && (
                                <div onClick={() => this.scrollToTopBtn()}>
                                    <span id="scroll-icon" className="material-icons">keyboard_arrow_up</span>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}


export default BooksItems
