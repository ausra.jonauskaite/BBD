import React, { useState } from 'react';
import { withGoogleMap, GoogleMap, Marker, InfoWindow, withScriptjs } from 'react-google-maps';
import { useDispatch, useSelector } from "react-redux";
import './MapsHooks.css';
import { Link, Redirect } from "react-router-dom";


function Map() {
    const maps = useSelector(state => state.maps);
    const [selectedItem, setSelectedItem] = useState(null);
    const [item_id, getID] = useState(null);
    // const mapRef = React.useRef(null);

    let coordinates = [];
    if (maps.coordinates !== undefined) {
        coordinates = maps.coordinates;
    }

    var center = new google.maps.LatLng(maps.coordinates.lat, maps.coordinates.lng);
    let id_array = [];
    let filterResults = [];

    maps.productsList.forEach(({ lat: lat, lng: lng, id: id }) => {
        let LatLng1 = new window.google.maps.LatLng(lat, lng);
        let result = google.maps.geometry.spherical.computeDistanceBetween(center, LatLng1);
        let id_Results = id;
        if (result < (maps.filterRadius * 1000)) {
            id_array.push(id_Results);
        }
    })

    filterResults = maps.productsList.filter((item) => id_array.includes(item.id));

    // bounds
    // if (maps.filterRadius === undefined) {
    //     maps.productsList.forEach(({ lat: lat, lng: lng }) => {
    //         const bounds = new window.google.maps.LatLngBounds();
    //         bounds.extend(new window.google.maps.LatLng(lat, lng));
    //         mapRef.current.fitBounds(bounds);
    //         console.log(bounds);
    //     })
    // } else {
    //     filterResults.forEach(({ lat: lat, lng: lng }) => {
    //         const bounds = new window.google.maps.LatLngBounds();
    //         bounds.extend(new window.google.maps.LatLng(lat, lng));
    //         mapRef.current.fitBounds(bounds);
    //         console.log(bounds);
    //     })
    // }

    return (
        <div>
            <GoogleMap

                defaultZoom={10}
                defaultCenter={{ lat: coordinates.lat, lng: coordinates.lng }}
            >
                {maps.coordinates.length !== 0 ? '' : (<div className="load-maps">Nepavyko užkrauti žemėlapio. Patikrinkite ar paskyroje nurodytas adresas.</div>)}

                <Marker position={{ lat: coordinates.lat, lng: coordinates.lng }}
                    icon={{
                        url: "/static/man.png",
                        scaledSize: new window.google.maps.Size(40, 40)
                    }}
                />

                {maps.filterRadius === undefined && (maps.productsList.map(data => (
                    <Marker key={data.id} position={{ lat: data.lat, lng: data.lng }}
                        onClick={() => {
                            setSelectedItem(data);
                        }}
                    />
                )))}


                {maps.filterRadius !== undefined && (filterResults.map(data => (
                    <Marker key={data.id} position={{ lat: data.lat, lng: data.lng }}
                        onClick={() => {
                            setSelectedItem(data);
                        }}
                    />
                )))}


                {selectedItem && (
                    <InfoWindow position={{ lat: selectedItem.lat, lng: selectedItem.lng }}
                        onCloseClick={() => {
                            setSelectedItem(null);
                        }}
                    >
                        <div className="window-container">
                            <img src={selectedItem.image} alt="" className="map-product-image" />
                            <p className="map-product-title">{selectedItem.name}</p>
                            <button type="button" onClick={() => { getID(selectedItem.id) }} className="map-product-Btn">PLAČIAU</button>
                        </div>
                    </InfoWindow>
                )}

                {item_id && (<Redirect to={{ pathname: "/preke_/" + item_id }} />)}


            </GoogleMap>
        </div >
    )
}

const WrappedMap = withScriptjs(withGoogleMap(Map));

export default function MapsHooks() {
    // const maps = useSelector(state => state.maps);
    // console.log(maps);
    return (
        <div style={{ width: '100%', height: '450px' }}>
            <WrappedMap
                googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyA3uCc5jzfhQ7JzCvpAf-wWljdJzICn0Y4`}
                loadingElement={<div style={{ height: "100%" }} />}
                containerElement={<div style={{ height: "100%" }} />}
                mapElement={<div style={{ height: "100%" }} />}
            />
        </div>
    );
}

