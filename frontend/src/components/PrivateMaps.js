import React, { Component } from 'react';
import './Maps.css';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';
import Geocode from "react-geocode";


class PrivateMaps extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items_address: this.props.items_filtered,
            location: [],
            errorMessage: '',
            set_radius: this.props.radius,
            user_address: this.props.usr_addr,
            user_lat: '',
            user_lng: ''
        }
    }

    // state = {
    //     items_address: this.props.items_filtered,
    //     location: [],
    //     errorMessage: '',
    //     set_radius: this.props.radius,
    //     user_address: this.props.usr_addr,
    //     user_lat: '',
    //     user_lng: ''
    // }

    componentDidMount() {
        Geocode.setApiKey("AIzaSyA3uCc5jzfhQ7JzCvpAf-wWljdJzICn0Y4");
        // this.getCoordinates();
        this.getUserCoordinates();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.items_filtered !== this.props.items_filtered || prevProps.radius !== this.props.radius) {
            this.setState({ items_address: this.props.items_filtered, set_radius: this.props.radius });
            console.log('check')
        }
    }


    getCoordinates = async () => {
        let result = this.props.items_filtered.map(a => a.location);
        console.log(this.state.items_address);
        // let location = await Geocode.fromAddress(result);
        // const { lat, lng } = location.results[0].geometry.location;
        // console.log(lat, lng);
        // this.setState({ location: { lat, lng } });
    }

    getUserCoordinates = async () => {
        let location = await Geocode.fromAddress(this.state.user_address);
        const { lat, lng } = location.results[0].geometry.location;
        this.setState({ user_lat: lat, user_lng: lng });
    }

    render() {

        // const { location } = this.state.location;
        const { user_lat, user_lng } = this.state;
        // console.log(location);

        return (
            <div>
                <Map
                    google={this.props.google}
                    zoom={17}
                    center={{ lat: user_lat, lng: user_lng }}
                    containerStyle={{ maxWidth: '900px', maxHeight: '450px' }}

                >
                    <Marker position={{ lat: user_lat, lng: user_lng }} />
                    {/* {location.map((marker) =>
                        <Marker position={{ lat: location.lat, lng: location.lng }} />)} */}
                </Map>
            </div>
        )
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyA3uCc5jzfhQ7JzCvpAf-wWljdJzICn0Y4'
})(PrivateMaps)