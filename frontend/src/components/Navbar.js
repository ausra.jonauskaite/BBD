import React, { Component } from "react";
import CategoryBar from './CategoryBar';
import CategoryBarAuth from './CategoryBarAuth';
import UserHeader from './UserHeader';
import Header from './Header';
import './Navbar.css';
import { connect } from "react-redux";
import PropTypes from "prop-types";


export class Navbar extends Component {

    static propTypes = {
        auth: PropTypes.object.isRequired
    }

    render() {

        const { isAuthenticated, user } = this.props.auth;

        const authLinks = (<UserHeader className="modal_index" />);
        const guestLinks = (<Header />);

        const authCategory = (<CategoryBarAuth className="modal_index" />);
        const guestCategory = (<CategoryBar />);

        return (
            <div className="navbar_position">
                { isAuthenticated ? authLinks : guestLinks}
                { isAuthenticated ? authCategory : guestCategory}
                {/* <CategoryBar /> */}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(Navbar);
