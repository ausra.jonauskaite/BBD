import React, { Component } from 'react';
import './UserHeader.css';
import { Link } from "react-router-dom";
import AvatarMenu from './AvatarMenu';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { logout, getMessages } from '../actions/auth';
import SearchBarPrivate from './SearchBarPrivate';


class UserHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isClicked: false,
        };
    }

    componentDidMount() {
        // const { isClicked } = this.state;
        this.props.getMessages();
        // this.setState({ isClicked: !isClicked });
    }


    state = { clicked: false }
    handleClick = () => {
        this.setState({ clicked: !this.state.clicked })
    }

    static propTypes = {
        auth: PropTypes.object.isRequired,
        user: PropTypes.object.isRequired,
        logout: PropTypes.func.isRequired,
        getMessages: PropTypes.func.isRequired
    };

    render() {

        const { isAuthenticated, user } = this.props.auth;

        // console.log(this.props.notifications);
        // const msg_count = Object.values(this.props.notifications.is_read && this.props.notifications.is_read).filter(a => a === true).length;
        // console.log(msg_count);

        return (
            <nav className="header">
                <div className="header_position">
                    {/*logo on the left*/}
                    <Link to="/" >
                        <img className="header_logo"
                            src="/static/Logo.png"
                            alt=""
                        />
                    </Link>

                    {/*search box*/}
                    <div className="header_search">
                        <SearchBarPrivate />
                    </div>


                    {/*upload item button with link*/}
                    <Link to='/ikelti_preke'>
                        <button className="header_uploadBtn">Įkelti prekę</button>
                    </Link>

                    <Link to="/zemelapis">
                        <div className="header_mapIcon">
                            <span className="material-icons md-22">map</span>
                        </div>
                    </Link>

                    <Link to="/pranesimai">
                        <div className="header_notificationIcon">
                            {this.props.notifications.filter(a => a.is_read === false).length !== 0 ? (<div className="notification_sum">{this.props.notifications.filter(a => a.is_read === false).length}</div>) : ''}
                            <span className="material-icons md-22">notifications</span>
                        </div>
                    </Link>

                    {/* user menu */}

                    <AvatarMenu user_img={this.props.user.image} className="avatar_menu" />

                    {/* logout */}
                    <div className="header_logoutIcon" onClick={this.props.logout}>
                        <span className="material-icons md-22">logout</span>
                    </div>

                    {/* menu for mobile device */}
                    <div className="mobile_menu" onClick={this.handleClick}>
                        <i id="menu_icon" className={this.state.clicked ? 'fas fa-times' : 'fas fa-bars'}></i>

                        <ul className={this.state.clicked ? 'nav_menu2 active' : 'nav_menu'}>
                            <Link to='/ikelti_preke' className="menu_item2 active">
                                <li>Įkelti prekę</li>
                            </Link>
                            <Link to='/profilis' className="menu_item2 active">
                                <li>Mano paskyra</li>
                            </Link>
                            <Link to='/mano_skelbimai' className="menu_item2 active">
                                <li>Mano skelbimai</li>
                            </Link>
                            <Link to='/pazymetos_prekes' className="menu_item2 active">
                                <li>Pažymėtos prekės</li>
                            </Link>
                            <Link to='/knygos' className="menu_item2 active">
                                <li>Knygos</li>
                            </Link>
                            <Link to='/stalo_zaidimai' className="menu_item2 active">
                                <li>Stalo žaidimai</li>
                            </Link>
                            <Link to='/dviraciai_paspirtukai' className="menu_item2 active">
                                <li>Dviračiai / paspirtukai</li>
                            </Link>
                            <Link to='/kitos_prekes' className="menu_item2 active">
                                <li>Kitos prekės</li>
                            </Link>
                            <Link to='/zemelapis' className="menu_item2 active">
                                <li>Žemėlapis</li>
                            </Link>
                            <Link to='/pranesimai' className="menu_item2 active">
                                <li>Pranešimai</li>
                            </Link>
                            <div className="menu_item2 active" onClick={this.props.logout}>
                                <li>Atsijungti</li>
                            </div>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.auth.user,
    notifications: state.auth.notifications
});

export default connect(mapStateToProps, { logout, getMessages })(UserHeader);
