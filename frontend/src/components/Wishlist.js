import React, { Component } from 'react';
import './UserAdlist.css';
import PrivateProduct from './PrivateProduct';
import { connect } from 'react-redux';
import { getWishlist } from '../actions/wishlist';
import PropTypes from "prop-types";
import './ScrollToTop.css';

class Wishlist extends Component {
    constructor(props) {
        super(props);
        this.state = {
            is_visible: false
        };
    }

    static propTypes = {
        getWishlist: PropTypes.func.isRequired,
        items: PropTypes.array.isRequired,
    }

    componentDidMount() {
        this.props.getWishlist();
        let scrollComponent = this;
        document.addEventListener("scroll", function (e) {
            scrollComponent.toggleVisibility();
        });
    }

    toggleVisibility() {
        if (window.pageYOffset > 300) {
            this.setState({
                is_visible: true
            });
        } else {
            this.setState({
                is_visible: false
            });
        }
    }

    scrollToTopBtn() {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    };

    render() {

        const { is_visible } = this.state;

        const wishlistItems = this.props.items && this.props.items.map((items, items_id) => (
            <PrivateProduct key={items_id}
                user_id={items.creator.id}
                item_id={items.id}
                userImage={items.creator.image}
                username={items.creator.username}
                itemImage={items.image}
                title={items.name}
                forSale={items.for_sale}
                creator_id={items.creator.id}
                inwishlist={items.in_wishlist}
            />
        )
        );

        return (
            <div className="main_cont">
                <div className="main_heading">
                    <div className="heading_box">
                        <div className="space_container">
                            <div className="title_container">
                                <h2 className="search_title">Pažymėtos prekės</h2>
                            </div>

                            <div className="filter_divider"></div>
                        </div>
                    </div>
                </div>
                {/*Wishlist item*/}
                <div className="items_row">
                    {wishlistItems}
                </div>
                <div className="scroll-to-top">
                    {is_visible && (
                        <div onClick={() => this.scrollToTopBtn()}>
                            <span id="scroll-icon" className="material-icons">keyboard_arrow_up</span>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    items: state.wishlist.items
});

export default connect(mapStateToProps, { getWishlist })(Wishlist)
