import React, { Component } from 'react';
import './EditUser.css';
import Form from "react-bootstrap/Form";
import PropTypes from 'prop-types';
import ReactDOM from "react-dom";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { updateUser } from '../actions/auth';
import store from '../store';
import { tokenConfig } from '../actions/auth';
import axios from 'axios';

class EditUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.user.id,
            username: this.props.user.username,
            email: this.props.user.email,
            phone_number: this.props.user.phone_number,
            address: this.props.user.address,
            image: this.props.user.image,
            blankError: '',
            updateSuccess: '',
        };
        this.handleImageChange = this.handleImageChange.bind(this);
    }

    state = {
        image: this.props.user.image,
        imgPreview: null,
        usernameError: "",
        emailError: "",
        numberError: "",
        addressError: "",
        isOpen: false,
        blankError: false,
        updateSuccess: false
    };

    // Username validation
    handleUserNameChange = event => {
        this.setState({ username: event.target.value }, () => {
            this.validateUserName();
        });
    };

    validateUserName = () => {
        const { username } = this.state;

        if (username.length < 5 || username.length > 24 || username.length == 0) {
            this.setState({ usernameError: 'Vardą turi sudaryti nuo 6 iki 24 simbolių' });
        } else {
            this.setState({ usernameError: '' });
        }
    };

    // Email validation
    handleEmailChange = event => {
        this.setState({ email: event.target.value }, () => {
            this.validateEmail();
        });
    };

    validateEmail = () => {
        const { email } = this.state;

        if (!email.includes('@') || email.length == 0) {
            this.setState({ emailError: 'Neteisingas el. pašto adresas' });
        } else {
            this.setState({ emailError: '' });
        }
    };

    // Phone number validation
    handlePhoneChange = event => {
        this.setState({ phone_number: event.target.value }, () => {
            this.validatePhone();
        });
    };

    validatePhone = () => {
        const { phone_number } = this.state;

        if (phone_number.length > 12 || phone_number.length < 12 || !phone_number.includes('+') || phone_number.length == 0) {
            this.setState({ numberError: 'Telefono numeris turi būti nurodytas tokiu formatu +3706' });
        } else {
            this.setState({ numberError: '' });
        }
    };

    // Address validation
    handleAddressChange = event => {
        this.setState({ address: event.target.value }, () => {
            this.validateAddress();
        });
    };

    validateAddress = () => {
        const { address } = this.state;

        if (address.length < 6 || address.length > 50 || address.length == 0) {
            this.setState({ addressError: 'Neteisingas nurodytas adresas' });
        } else {
            this.setState({ addressError: '' });
        }
    };


    // update image
    handleImageChange(event) {
        this.setState({
            image: event.target.files[0],
            imgPreview: URL.createObjectURL(event.target.files[0]),
        })
    };

    finalValidation = () => {
        const { usernameError, emailError, numberError, addressError } = this.state;
        if ((usernameError === undefined || usernameError === '') && (emailError === undefined || emailError === '') && (numberError === undefined || numberError === '') && (addressError === undefined || addressError === '')) {
            return true;
        } else {
            this.setState({ blankError: true });
            setTimeout(function () { this.setState({ blankError: false }) }.bind(this), 4000);
            return false;
        }

        // if (usernameError != '' && emailError != '' && numberError != '' && addressError != '') {
        //     this.setState({ blankError: true });
        //     setTimeout(function () { this.setState({ blankError: false }) }.bind(this), 4000);
        //     return false;
        // }
        // return true;
    }

    // onSubmit
    onSubmit = (event) => {

        event.preventDefault();
        // additional form validation
        const isValid = this.finalValidation();

        if (isValid) {
            const imgPrev = this.state.imgPreview;

            if (!imgPrev) {
                const formData = new FormData();
                formData.append('email', this.state.email);
                formData.append('username', this.state.username);
                formData.append('phone_number', this.state.phone_number);
                formData.append('address', this.state.address);

                // console.log(Object.fromEntries(formData));
                this.updateUser(formData);
                this.props.updateUser(formData);

            } else {
                const formData = new FormData();
                formData.append('email', this.state.email);
                formData.append('username', this.state.username);
                formData.append('phone_number', this.state.phone_number);
                formData.append('address', this.state.address);
                formData.append('image', this.state.image);

                // console.log(Object.fromEntries(formData));
                this.updateUser(formData);
                this.props.updateUser(formData);
            }
        }

    };

    updateUser(formData) {
        axios
            .patch('/api/auth/update', formData, tokenConfig(store.getState))
            .then(response => { this.setState({ successUpdate: response.status }) })
            .catch(err => { this.setState({ blankError: err.response.status }) });
    };

    static propTypes = {
        updateUser: PropTypes.func.isRequired
    }

    render() {

        if (this.state.status === 404 || this.state.status === 500) {
            return <Redirect to="/404" />
        }


        const { username, email, phone_number, address, image, imgPreview } = this.state;

        return (
            <div>
                <div className="error_container">
                    <div className="sticky-container-item form_container">
                        <div className="error_container">
                            {this.state.blankError || this.state.status === 400 ? (<div className="alertMsg_container fade-in" ><div className="alert_msg">
                                <p className="msg_cont">Klaida! Paskyra negali būti atnaujinta</p>
                            </div></div>) : ''}

                            {this.state.successUpdate === 200 ? (<div className="alertMsg_container fade-in" ><div className="alertPsw_msg">
                                <p className="msg_cont">Paskyra atnaujinta sėkmingai</p>
                            </div></div>) : ''}
                        </div>
                    </div>
                </div>
                <div className="main_box">
                    <div className="form_box">
                        <Form className="form_content" onSubmit={this.onSubmit.bind(this)}>

                            <div className="user_image">
                                {this.state.imgPreview ?
                                    <img src={imgPreview} alt="user picture here" />
                                    : <img src={image} alt="" />}
                            </div>

                            <div className="image_upload2">
                                <input className="file" type="file" id="img_file" accept="image/png, image/jpeg, image/jpg" name="image" onChange={this.handleImageChange} />
                                <label className="upload_btn1" htmlFor="img_file">Įkelti nuotrauką</label>
                            </div>

                            <div className="formats_msg">
                                <p>Palaikomi nuotraukų formatai: JPEG, JPG, PNG</p>
                                <p>Dydis: 1280x720</p>
                            </div>

                            <div className="section_divider"></div>

                            <div className="dbox_container">
                                <label className="label_title">
                                    Vardas
                        </label>
                                <input type="text" className="input_container" name="username" value={username} onChange={this.handleUserNameChange} onBlur={this.validateUserName} />
                                <div className='input_errors'>{this.state.usernameError}</div>

                                <label className="label_title">
                                    El. paštas
                        </label>
                                <input type="email" className="input_container" name="email" value={email} onChange={this.handleEmailChange} onBlur={this.validateEmail} />
                                <div className='input_errors'>{this.state.emailError}</div>

                                <label className="label_title">
                                    Telefono numeris
                        </label>
                                <input type="tel" className="input_container" name="phone_number" value={phone_number} onChange={this.handlePhoneChange} onBlur={this.validatePhone} />
                                <div className='input_errors'>{this.state.numberError}</div>
                                <label className="label_title">
                                    Adresas
                        </label>
                                <input type="text" className="input_container" name="address" value={address} onChange={this.handleAddressChange} onBlur={this.validateAddress} />
                                <div className='input_errors'>{this.state.addressError}</div>
                            </div>


                            <div className="btn_position">
                                <Link to="/profilis">
                                    <button className="cancelation_btn">Atgal</button>
                                </Link>
                                <button className="validation_btn" type="submit">Išsaugoti</button>

                            </div>
                        </Form>
                    </div>
                </div >
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.auth.user,
    errors: state.errors
});

export default connect(mapStateToProps, { updateUser })(withRouter(EditUser));
