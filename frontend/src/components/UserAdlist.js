import React, { Component } from 'react';
import './UserAdlist.css';
import UserProducts from './UserProducts';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getUserItems } from '../actions/userinfo';
import './ScrollToTop.css';

class UserAdlist extends Component {
    constructor(props) {
        super(props);
        this.state = {
            is_visible: false
        };
    }

    static propTypes = {
        items: PropTypes.array.isRequired,
        getUserItems: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.props.getUserItems();
        let scrollComponent = this;
        document.addEventListener("scroll", function (e) {
            scrollComponent.toggleVisibility();
        });
    }

    // componentDidUpdate(prevProps) {
    //     if (prevProps.items.length !== this.props.items.length) {
    //         this.props.getUserItems();
    //     }
    // }

    toggleVisibility() {
        if (window.pageYOffset > 300) {
            this.setState({
                is_visible: true
            });
        } else {
            this.setState({
                is_visible: false
            });
        }
    }

    scrollToTopBtn() {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    };

    render() {

        const { is_visible } = this.state;

        return (
            <div className="main_cont">
                <div className="main_heading">
                    <div className="heading_box">
                        <div className="space_container">
                            <div className="title_container">
                                <h2 className="search_title">Mano skelbimai</h2>
                            </div>

                            <div className="filter_divider"></div>
                        </div>
                    </div>
                </div>
                <div className="items_row">
                    {this.props.items.map(items => (
                        <UserProducts key={items.id}
                            item_id={items.id}
                            itemImage={items.image}
                            title={items.name}
                            forSale={items.for_sale}
                        />
                    ))}
                </div>
                <div className="scroll-to-top">
                    {is_visible && (
                        <div onClick={() => this.scrollToTopBtn()}>
                            <span id="scroll-icon" className="material-icons">keyboard_arrow_up</span>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    items: state.userinfo.items
});

export default connect(mapStateToProps, { getUserItems })(UserAdlist)
