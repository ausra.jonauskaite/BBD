import React from 'react';
import './ItemForm.css';
import FormContent from './FormContent';

function ItemForm() {
    return (
        <div className="form_container">
            {/* <h2 className="form_headline">Įkelti prekę</h2> */}
            <FormContent />
        </div>
    )
}

export default ItemForm
