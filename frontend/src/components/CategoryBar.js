import React from 'react'
import './CategoryBar.css';
import { Link } from "react-router-dom";

function CategoryBar() {
    return (
        <div className="categoryBar">
            <div className="categoryBar_position">
                <div className="categoryBar_txtPosition">
                    <Link to='/knygos' className="element_position">Knygos</Link>
                    <Link to='/stalo_zaidimai' className="element_position">Stalo žaidimai</Link>
                    <Link to='/dviraciai_paspirtukai' className="element_position">Dviračiai / paspirtukai</Link>
                    <Link to='/kitos_prekes' className="element_position">Kitos prekės</Link>
                </div>
                <div className="line"></div>
            </div>
        </div>
    )
}

export default CategoryBar
