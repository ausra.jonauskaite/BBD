import React, { Component } from 'react';
import Helmet from 'react-helmet';
import ModifyFormContent from './ModifyFormContent';
import Navbar from './Navbar';

class ModifyItem extends Component {

    constructor(props) {
        super(props);

    };
    componentDidMount() {
        const id = this.props.match.params.item_id;
    }

    render() {
        return (
            <div>
                <Navbar />
                <ModifyFormContent />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />

            </div>
        )
    }
}

export default ModifyItem
