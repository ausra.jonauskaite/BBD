import React, { Component } from 'react';
import './PageNotFound.css';
import { Link } from "react-router-dom";



export class PageNotFound extends Component {
    render() {
        return (
            <div>
                <div className="card__container">
                    <div className="card__item">
                        <img src="/static/20945761.jpg" alt="" />
                        <p className="card__info__whoops">Whoops!</p>
                        <p className="card__info">Puslapis, kurios ieškote neegzistuoja.</p>

                        <Link to="/"><button className="card__button">Grįžti į pradinį puslapį</button></Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default PageNotFound
