import React, { Component } from 'react';
import Navbar from "../Navbar";
import ExpandProductPrivate from "../ExpandProductPrivate";
import Helmet from 'react-helmet';

export class PrivateProductRoute extends Component {

    constructor(props) {
        super(props);

    };
    componentDidMount() {
        const id = this.props.match.params.item_id;
    }

    render() {
        return (
            <div>
                <Navbar />
                <ExpandProductPrivate />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default PrivateProductRoute
