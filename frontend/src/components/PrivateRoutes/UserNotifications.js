import React, { Component } from 'react';
import Helmet from 'react-helmet';
import Navbar from '../Navbar';
import Notifications from '../Notifications';

export class UserNotifications extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <Notifications />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default UserNotifications
