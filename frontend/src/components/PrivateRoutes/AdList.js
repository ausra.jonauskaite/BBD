import React, { Component } from 'react';
import Helmet from 'react-helmet';
import Navbar from '../Navbar';
import UserAdlist from '../UserAdlist';

export class AdList extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <UserAdlist />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default AdList
