import React, { Component } from 'react';
import Helmet from 'react-helmet';
import Navbar from '../Navbar';
import ItemForm from '../ItemForm';

export class AddItem extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <ItemForm />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default AddItem
