import React, { Component } from 'react';
import Helmet from 'react-helmet';
import Maps from '../Maps';
import Navbar from '../Navbar';


export class UserMap extends Component {


    render() {
        return (
            <div>
                <Navbar />
                <Maps />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default UserMap
