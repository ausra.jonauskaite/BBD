import React, { Component } from 'react';
import Helmet from 'react-helmet';
import PrivateItemsSearch from '../PrivateItemsSearch';
import Navbar from '../Navbar';



export class PrivateSearch extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <PrivateItemsSearch />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default PrivateSearch
