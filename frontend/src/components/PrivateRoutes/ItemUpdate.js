import React, { Component } from 'react';
import Helmet from 'react-helmet';
import EditItem from '../EditItem';
import Navbar from '../Navbar';

export class ItemUpdate extends Component {
    constructor(props) {
        super(props);

    };
    componentDidMount() {
        const id = this.props.match.params.item_id;
    }
    render() {
        return (
            <div>
                <Navbar />
                <EditItem />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default ItemUpdate
