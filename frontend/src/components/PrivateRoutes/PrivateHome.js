import React, { Component } from 'react'
// import '../Home.css';
import HomeItemsPrivate from '../HomeItemsPrivate';
import Slider from "../Slider";
import Headline from "../Headline";


class PrivateHome extends Component {

    render() {
        return (
            <div>
                {/*Product*/}
                {/* <div className="home_row"> */}
                <Slider />
                <Headline />
                <HomeItemsPrivate />
                {/* </div> */}
            </div>
        )
    }
}


export default PrivateHome