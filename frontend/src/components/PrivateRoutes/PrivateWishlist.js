import React, { Component } from 'react';
import Navbar from "../Navbar";
import Wishlist from "../Wishlist";
import Helmet from 'react-helmet';


export class PrivateWishlist extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <Wishlist />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default PrivateWishlist
