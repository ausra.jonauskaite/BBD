import React, { Component } from 'react';
import ModifyItem from '../ModifyItem';
import { Navbar } from '../Navbar';


export class ItemModification extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <ModifyItem />
            </div>
        )
    }
}

export default ItemModification
