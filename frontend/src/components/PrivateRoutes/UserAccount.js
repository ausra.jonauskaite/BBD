import React, { Component } from 'react';
import Helmet from 'react-helmet';
import Navbar from '../Navbar';
import UserProfile from '../UserProfile';

export class UserAccount extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <UserProfile />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default UserAccount
