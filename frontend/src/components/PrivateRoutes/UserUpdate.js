import React, { Component } from 'react';
import Helmet from 'react-helmet';
import Navbar from '../Navbar';
import EditUser from '../EditUser';

export class UserUpdate extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <EditUser />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}

export default UserUpdate
