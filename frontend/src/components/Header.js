import React, { Component } from 'react';
import './Header.css';
import { Link } from "react-router-dom";
import axios from 'axios';
import SearchBarPublic from './SearchBarPublic';
// import JSONDATA from './MOCK_DATA_PRODUCT.json';
// import Product from './Product';

class Header extends Component {

    state = { clicked: false }
    handleClick = () => {
        this.setState({ clicked: !this.state.clicked })
    }

    render() {

        return (
            <nav className="header">
                <div className="header_position">
                    {/*logo on the left*/}
                    <Link to="/">
                        <img className="header_logo"
                            src={"/static/Logo.png"}
                            alt=""
                        />
                    </Link>

                    {/*search box*/}
                    <div className="header_search">
                        <SearchBarPublic />
                    </div>


                    {/*login button with link*/}
                    <Link to='/prisijungimas'>
                        <button className="header_loginBtn">Prisijungti</button>
                    </Link>

                    {/* menu for mobile device */}
                    <div className="mobile_menu" onClick={this.handleClick}>
                        <i id="menu_icon" className={this.state.clicked ? 'fas fa-times' : 'fas fa-bars'}></i>

                        <ul className={this.state.clicked ? 'nav_menu active' : 'nav_menu'}>
                            <Link to='/prisijungimas' className="menu_item active">
                                <li>Prisijungti</li>
                            </Link>
                            <Link to='/knygos' className="menu_item active">
                                <li>Knygos</li>
                            </Link>
                            <Link to='/stalo_zaidimai' className="menu_item active">
                                <li>Stalo žaidimai</li>
                            </Link>
                            <Link to='/dviraciai_paspirtukai' className="menu_item active">
                                <li>Dviračiai / paspirtukai</li>
                            </Link>
                            <Link to='/kitos_prekes' className="menu_item active">
                                <li>Kitos prekės</li>
                            </Link>
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
}

export default Header
