import React, { Component } from 'react';
import './UserHeader.css';
import PropTypes from "prop-types";
import { Link, withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { privateSearch } from '../actions/search';

export class SearchBarPrivate extends Component {

    constructor(props) {
        super(props);
        this.state = {
            search: "",
            search_LN: "",
            term: ""
        }
    }

    textFormatter(txt) {
        let newValue = txt.toLowerCase()
            .replace("ą", "a")
            .replace("č", "c")
            .replace("ę", "e")
            .replace("ė", "e")
            .replace("į", "i")
            .replace("š", "s")
            .replace("ų", "u")
            .replace("ū", "u")
            .replace("ž", "z")
            .replace(/[/=-_)(*£"¬`;|~'"?.,+!@#$%^&*]+/g, "")

        return newValue
    }

    termFormatter(txt) {
        let term_value = txt.toLowerCase()
            .replace(/[/=-_)(*£"¬`;'"?|~+.,!@#$%^&*]+/g, "")

        return term_value
    }

    handleSearch = (e) => this.setState({ search: e.target.value })

    handleSubmit = (e) => {
        const { search } = this.state;
        let newUrl = this.textFormatter(search);
        let newTerm = this.termFormatter(search);
        this.setState({ term: newTerm, search_LN: newUrl, search: "" });
        // this.props.privateSearch(newTerm);
    }

    static propTypes = {
        privateSearch: PropTypes.func.isRequired
    };

    render() {

        const { search } = this.state;

        return (
            <div className="header_search">
                <input type="text" className="header_searchInput" placeholder="Ieškoti" value={search} onChange={this.handleSearch} maxLength="60" />
                <div className="header_searchIcon">
                    <div onClick={this.handleSubmit} className="material-icons">search</div>
                </div>

                {this.state.term.length > 0 &&
                    <Redirect to={{
                        pathname: '/products-logged?search=' + this.state.search_LN,
                        state: { term: this.state.term }
                    }} />
                }

            </div>
        )
    }
}

const mapStateToProps = state => ({
    name: state.search
});

export default connect(mapStateToProps, { privateSearch })(withRouter(SearchBarPrivate))
