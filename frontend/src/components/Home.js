import React, { Component } from 'react'
import './Home.css';
import HomeItemsPublic from './HomeItemsPublic';

class Home extends Component {

    render() {
        return (
            <div className="home">
                {/*Product*/}
                {/* <div className="home_row"> */}
                <HomeItemsPublic />
                {/* </div> */}
            </div>
        )
    }
}


export default Home