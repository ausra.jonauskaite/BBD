import React, { useEffect, useRef } from "react";
import styled from "styled-components";
import posed from "react-pose";

function useOnClickOutside(ref, handler) {
    useEffect(() => {
        const listener = event => {
            // Do nothing if clicking ref's element or descendent elements
            if (!ref.current || ref.current.contains(event.target)) {
                return;
            }

            handler(event);
        };

        document.addEventListener("mousedown", listener);
        document.addEventListener("touchstart", listener);

        return () => {
            document.removeEventListener("mousedown", listener);
            document.removeEventListener("touchstart", listener);
        };
    }, []); // Empty array ensures that effect is only run on mount and unmount
}

const menuBackgroundPoses = {
    open: {
        background: "rgba(0, 0, 0, 0)",
        applyAtStart: {
            display: "block"
        }
    },
    closed: {
        background: "rgba(0, 0, 0, 0)",
        applyAtEnd: {
            display: "none"
        }
    }
};

const MenuBackground = styled(posed.div(menuBackgroundPoses))`
  position: fixed;
  display: flex;
  top: 0;
  left:0
  max-width: 1100px;
  height: 100%;
`;

const menuPoses = {
    open: {
        opacity: 1,
        transition: {
            opacity: {
                type: "tween",
                duration: 200
            }
        }
    },
    closed: {
        opacity: 0,
        transition: {
            opacity: {
                type: "tween",
                duration: 200
            }
        }
    }
};

const Menu = styled(posed.div(menuPoses))`
 
text-align: left;
  position: relative;
  background: white;
  height: 140px;
  width: 200px;
  top: 60px;
  left: -78%;
 //transform: translate(-50%, -50%);
  border-radius: 5px;
  padding: 20px;
  box-shadow: 0 2px 4px 0 rgba(50, 50, 93, 0.1);
  z-index: 99999;
  transition: ease-out 0.1s;

  @media screen and (max-width: 768px) {
    display: none;
  }

  @media screen and (max-width: 468px) {
      display: none;
  }
`;

export default function ({ isOpen, toggle, children }) {
    const ref = useRef();

    useOnClickOutside(ref, () => toggle(false));

    return (
        <MenuBackground initialPose="closed" pose={isOpen ? "open" : "closed"}>
            <Menu ref={ref}>{children}</Menu>
        </MenuBackground>
    );
}

