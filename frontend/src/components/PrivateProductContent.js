// import React from 'react';
// import './PrivateProductContent.css';
// import ReactDOM from "react-dom";
// import { Link } from "react-router-dom";
// import PrivateProductForm from './PrivateProductForm';
// import { useDispatch, useSelector } from "react-redux";
// import PrivateMap from './PrivateMap';
// import './ItemMap.css';


// function PrivateProductContent({ item_id, itemImage, title, forSale, quality, primary_value, category, location, creation_date, price, description, userImage, username, inwishlist }) {

//     const user = useSelector(state => state.auth.user);
//     const creator = useSelector(state => state.product.privateproduct.creator);
//     const itemId = useSelector(state => state.product.privateproduct);
//     const sent = useSelector(state => state.send);
//     const notSent = useSelector(state => state.errors);


//     return (
//         <div>
//             <div className="content_container">
//                 <div className="main_container">



//                     <div>
//                         <img src={itemImage} alt="" className="product_image" />
//                     </div>

//                     <div className="content_cont">
//                         <div className="product_information">
//                             <div className="product_name">
//                                 {forSale ? <span className="material-icons md-28">monetization_on</span> : ""}
//                                 <p>{title}</p>
//                             </div>
//                             <div className="content_divider"></div>

//                             <div className="info_display">
//                                 <div className="txt_display">
//                                     <p>BŪKLĖ</p>
//                                     <p>PRADINĖ VERTĖ</p>
//                                     <p>KATEGORIJA</p>
//                                     <p>ADRESAS</p>
//                                     <p>ĮKĖLIMO DATA</p>
//                                     <p>KAINA</p>
//                                 </div>

//                                 <div className="txt_display">
//                                     <p>{quality}</p>
//                                     <p>{primary_value} &#x20AC;</p>
//                                     <p>{category}</p>
//                                     <p>{location}</p>
//                                     <p>{creation_date}</p>
//                                     <p>{price} &#x20AC;</p>
//                                 </div>
//                             </div>
//                             {/* <PrivateProductForm /> */}
//                         </div>
//                     </div>
//                 </div>

//                 <div className="dbox_info">
//                     <p className="product_d">Aprašymas</p>
//                     <p className="product_dbox">{description}</p>
//                     <img src={userImage} alt="" />
//                     <p className="usr_name">{username}</p>
//                     <div className="content_div"></div>
//                 </div>
//             </div>
//             <div className="map_container">
//                 <div className="map_box">
//                     <div className="map_display2">
//                         <PrivateMap address={location} />
//                     </div>
//                     <div className="form_display">
//                         <Link to="/zemelapis">
//                             <div className="center_btn">
//                                 <button className="search_btn3">Ieškoti daugiau prekių</button>
//                             </div>
//                         </Link>
//                     </div>
//                 </div>
//             </div>
//         </div>
//     )
// }

// export default PrivateProductContent
