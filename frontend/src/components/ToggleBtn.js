import React, { Component } from 'react';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import './HeartBtn.css';
import _ from 'lodash';

class ToggleBtn extends Component {

    static propTypes = {
        auth: PropTypes.object.isRequired
    }

    render() {

        // console.log(this.state.wishes);
        const { isAuthenticated, user } = this.props.auth;
        const listItem = this.props.wishes && this.props.wishes.map((v) => {
            return (
                <div> { isAuthenticated ? <span className="material-icons" id="outline_hearts" onClick={() => this.props.toggleActive(v.id)}>favorite_border</span> : ""}
                    {v.have ? <span className="material-icons" id="full_hearts" onClick={() => this.props.toggleActive(v.id)}>favorite</span> : ""}
                </div>
            );
        })

        // console.log(this.state.wishes)
        // console.log(listItem);
        // console.log(this.props.items.id);
        // console.log(this.props.wishes.id);
        return (
            <div>
                {/* { isAuthenticated ? <span className="material-icons" id="outline_hearts" onClick={() => this.props.toggleActive(this.props.wishes.id)}>favorite_border</span> : ""} */}

                {/* {this.props.wishes.have ? <span className="material-icons" id="full_hearts" onClick={() => this.props.toggleActive(this.props.wishes.id)}>favorite</span> : ""} */}
                {/* <span class="material-icons" id="hearts">favorite</span> */}
                {listItem}

            </div>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.auth.user
});

export default connect(mapStateToProps)(ToggleBtn)