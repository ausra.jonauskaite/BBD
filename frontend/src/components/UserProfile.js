import React, { Component } from 'react';
import './UserProfile.css';
import Form from "react-bootstrap/Form";
import PswModal from './PswModal';
import ReactDOM from "react-dom";
import EditUser from './EditUser';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { getUserinfo } from '../actions/userinfo';
import store from '../store';
import { tokenConfig } from '../actions/auth';
import axios from 'axios';


class UserProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            blankError: '',
            updateSuccess: ''
        };
    }

    static propTypes = {
        userinfo: PropTypes.array.isRequired,
        getUserinfo: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.getUserinfo();
        this.setState({ isLoading: false });
    }

    componentDidUpdate(prevProps) {
        const { error } = this.props;
        if (error !== prevProps.error) {
            this.setState({ incorrectPswError: true });
            setTimeout(function () { this.setState({ incorrectPswError: false }) }.bind(this), 4000);
            // console.log('checking');
        }
    }

    updatePassword(body) {
        axios
            .post('/api/auth/change', body, tokenConfig(store.getState))
            .then(response => { this.setState({ updateSuccess: response.status }) })
            .catch(err => { this.setState({ blankError: err.response.status }) });
    }

    state = {
        //   id: this.props.userinfo.id,
        old_password: "",
        password: "",
        password2: "",
        isOpen: false,
        oldPasswordError: "",
        newPasswordError: "",
        newPasswordError2: "",
        blankError: false,
        incorrectPswError: false
    };

    toggleModal = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    // Password Validation
    // Old Psw
    handleOldPswChange = event => {
        this.setState({ old_password: event.target.value }, () => {
            this.validateOldPsw();
        });
    };

    validateOldPsw = () => {
        const { old_password } = this.state;

        if (this.state.isOpen) {
            if (old_password.length < 6 || old_password.length > 24 || old_password.length == 0) {
                this.setState({ oldPasswordError: 'Slaptažodis turi būti tarp 6-24 simbolių' });
            } else {
                this.setState({ oldPasswordError: '' });
            }
        }
    };

    // New Psw
    handleNewPswChange = event => {
        this.setState({ password: event.target.value }, () => {
            this.validateNewPsw();
        });
    };

    validateNewPsw = () => {
        const { password } = this.state;

        if (this.state.isOpen) {
            if (password.length < 6 || password.length > 24 || password.length == 0) {
                this.setState({ newPasswordError: 'Slaptažodis turi būti tarp 6-24 simbolių' });
            } else {
                this.setState({ newPasswordError: '' });
            }
        }
    };

    // New Psw confirmation
    handleNewPsw2Change = event => {
        this.setState({ password2: event.target.value }, () => {
            this.validateNewPsw2();
        });
    };

    validateNewPsw2 = () => {
        const { password2, password } = this.state;

        if (this.state.isOpen) {
            if (password2 !== password || password2.length == 0) {
                this.setState({ newPasswordError2: 'Neteisingai įvestas slaptažodis' });
            } else {
                this.setState({ newPasswordError2: '' });
            }
        }
    };

    // pswSubmit form
    pswSubmit = (e) => {
        e.preventDefault();
        const { old_password, password, password2 } = this.state;

        if (password !== password2 || old_password == 0 || password == 0 || password2 == 0) {
            this.setState({ blankError: true });
            setTimeout(function () { this.setState({ blankError: false }) }.bind(this), 3000);
        } else {
            const newUserPassword = {
                old_password,
                password,
                password2
            };
            this.updatePassword(newUserPassword);
            this.toggleModal(false);
        }
    };


    render() {

        const { id, old_password, password, password2, isLoading } = this.state;

        if (isLoading) {
            return <div className="loading_display">Loading...</div>;
        }

        return (
            <div>
                <div className="error_container">
                    <div className="sticky-container-item form_container">
                        <div className="error_container">
                            {this.state.blankError || this.state.status === 400 ? (<div className="alertMsg_container fade-in" ><div className="alert_msg">
                                <p className="msg_cont">Klaida! Neteisingai nurodyti duomenys</p>
                            </div></div>) : ''}

                            {this.state.updateSuccess ? (<div className="alertMsg_container fade-in" ><div className="alertPsw_msg">
                                <p className="msg_cont">Slaptažodis atnaujintas sėkmingai</p>
                            </div></div>) : ''}
                        </div>
                    </div>
                </div>

                <div className="main_box">
                    <div className="form_box">
                        <div className="form_content">

                            <div className="usr_image">
                                <img src={this.props.userinfo.image} alt="" />
                            </div>


                            <div className="section_divider"></div>

                            <div className="dbox_container">
                                <label className="label_title">
                                    Vardas
                        </label>
                                <p type="text" className="input_container" name="username">{this.props.userinfo.username}</p>

                                <label className="label_title">
                                    El. paštas
                        </label>
                                <p type="email" className="input_container" name="email">{this.props.userinfo.email}</p>


                                <label className="label_title">
                                    Telefono numeris
                        </label>
                                <p type="tel" className="input_container" name="number">{this.props.userinfo.phone_number}</p>

                                {this.state.incorrectPswError ? (<div className="alertMsg_container2 fade-in" ><div className="alert_msg">
                                    <p className="msg_cont">Klaida! Neteisingai nurodytas senas slaptažodis</p>
                                </div></div>) : ''}

                                <label className="label_title">
                                    Slaptažodis
                        </label>

                                <div className="modbtn_position">
                                    <button className="mod_btn" onClick={this.toggleModal}>Pakeisti slaptažodį</button>
                                </div>

                                <label className="label_title">
                                    Adresas
                        </label>
                                <p type="text" className="input_container" name="address">{this.props.userinfo.address}</p>

                            </div>

                            <div className="btn_position">
                                <Link to="/paskyros_redagavimas">
                                    <button className="validation_btn">Redaguoti</button>
                                </Link>
                            </div>
                        </div>

                        <Form onSubmit={this.pswSubmit}>
                            <PswModal show={this.state.isOpen}>

                                <div className="error_container">
                                    {this.state.blankError ? (<div className="fade-in" ><div className="alert_msg">
                                        <p className="msg_cont">Klaida! Neteisingai nurodyti duomenys</p>
                                    </div></div>) : ''}
                                </div>

                                <label className="label_title">
                                    Senas slaptažodis
                        </label>
                                <input type="password" className="psw_modification" name="old_password" value={old_password} onChange={this.handleOldPswChange} onBlur={this.validateOldPsw} />
                                <div className='input_errors'>{this.state.oldPasswordError}</div>

                                <label className="label_title">
                                    Naujas slaptažodis
                        </label>
                                <input type="password" className="psw_modification" name="password" value={password} onChange={this.handleNewPswChange} onBlur={this.validateNewPsw} />
                                <div className='input_errors'>{this.state.newPasswordError}</div>

                                <label className="label_title">
                                    Pakartokite slaptažodį
                        </label>
                                <input type="password" className="psw_modification" name="password2" value={password2} onChange={this.handleNewPsw2Change} onBlur={this.validateNewPsw2} />
                                <div className='input_errors'>{this.state.newPasswordError2}</div>

                                <div className="psw_modiifybtn">
                                    <button className="cancel_Btn" onClick={this.toggleModal}>Atšaukti</button>
                                    <button className="valid_Btn" onSubmit={this.pswSubmit}>Patvirtinti</button>
                                </div>
                            </PswModal>
                        </Form>

                    </div>
                </div >
            </div>
        );
    }
}

const mapStateToProps = state => ({
    userinfo: state.userinfo.user,
    error: state.errors
});

export default connect(mapStateToProps, { getUserinfo })(UserProfile);
