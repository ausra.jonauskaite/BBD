// import React, { Component } from 'react';
// import { connect } from "react-redux";
// import PropTypes from "prop-types";
// import './HeartBtn.css';
// import _ from 'lodash';

// class ToggleBtn2 extends Component {

//     static propTypes = {
//         auth: PropTypes.object.isRequired
//     }

//     render() {


//         const { isAuthenticated, user } = this.props.auth;
//         const listItem = this.props.wishes && this.props.wishes.map((v) => {
//             return (
//                 <div> { isAuthenticated ? <span className="material-icons" id="outline_hearts" onClick={() => this.props.toggleActive(v.id)}>favorite_border</span> : ""}
//                     {v.have ? <span className="material-icons" id="full_hearts" onClick={() => this.props.toggleActive(v.id)}>favorite</span> : ""}
//                 </div>
//             );
//         });

//         return (
//             <div>
//                 {listItem}
//             </div>
//         )
//     }
// }

// const mapStateToProps = state => ({
//     auth: state.auth,
//     user: state.auth.user
// });

// export default connect(mapStateToProps)(ToggleBtn2)