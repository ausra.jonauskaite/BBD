import React from 'react';
import './Product.css';
import { Link } from "react-router-dom";


function UserProducts({ itemImage, forSale, title, item_id }) {

    return (
        <div className="product">
            <div className="product_info">

                <Link to={"/preke_/" + item_id} className="product_info">
                    <img src={itemImage} alt="" />
                </Link>

                <div className="product_title">
                    {forSale ?
                        <span className="material-icons">paid</span>
                        : ""
                    }
                    <p>{title}</p>
                </div>

            </div>
        </div>
    )
}


export default UserProducts
