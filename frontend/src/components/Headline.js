import React from 'react'
import './Headline.css';

function Headline() {
    return (
        <div className="headline_body">
            <div className="headline">
                <h2 className="headline_prop">Naujausi pasiūlymai</h2>
            </div>

            <div className="line"></div>

        </div>
    )
}

export default Headline
