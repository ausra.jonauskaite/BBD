import React, { Component } from 'react';
import './ItemsNotFound.css'

export class ItemsNotFound extends Component {
    render() {
        return (
            <div>
                <p className="items_notfound">Atsiprašome, prekių nerasta.</p>
            </div>
        )
    }
}

export default ItemsNotFound
