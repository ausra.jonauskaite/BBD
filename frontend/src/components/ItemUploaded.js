import React, { Component } from 'react';
import Navbar from './Navbar';
import ItemCard from './ItemCard';
import Helmet from 'react-helmet';


export class ItemUploaded extends Component {
    render() {
        return (
            <div>
                <Navbar />
                <ItemCard />
                <Helmet bodyAttributes={{ style: 'background-color : #f9f9f9' }} />
            </div>
        )
    }
}


export default ItemUploaded
