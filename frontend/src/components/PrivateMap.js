import React, { Component } from 'react';
import './Maps.css';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';
import Geocode from "react-geocode";


class PrivateMap extends Component {

    state = {
        addr: this.props.address,
        latitude: '',
        longitude: '',
        errorMessage: ''
    }

    componentDidMount() {
        // this is needed for geocode
        Geocode.setApiKey("AIzaSyA3uCc5jzfhQ7JzCvpAf-wWljdJzICn0Y4");
        this.getCoordinates();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.address !== this.props.address) {
            Geocode.setApiKey("AIzaSyA3uCc5jzfhQ7JzCvpAf-wWljdJzICn0Y4");
            this.getUpdatedCoordinates(this.props.address);
        }
    }

    getCoordinates = async () => {
        let location = await Geocode.fromAddress(this.state.addr);
        const { lat, lng } = location.results[0].geometry.location;
        this.setState({ latitude: lat, longitude: lng });
    }

    getUpdatedCoordinates = async (address) => {
        let location = await Geocode.fromAddress(address);
        const { lat, lng } = location.results[0].geometry.location;
        this.setState({ latitude: lat, longitude: lng });
    }

    render() {

        const { latitude, longitude } = this.state;

        return (
            <div>
                <Map
                    google={this.props.google}
                    zoom={17}
                    center={{ lat: latitude, lng: longitude }}
                    containerStyle={{ width: '100%', height: '300px', left: 'auto', right: 'auto', position: 'relative' }}
                    disableDefaultUI={true}
                    zoomControl={true}
                >
                    <Marker position={{ lat: latitude, lng: longitude }} />
                </Map>
            </div>
        )
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyA3uCc5jzfhQ7JzCvpAf-wWljdJzICn0Y4'
})(PrivateMap)