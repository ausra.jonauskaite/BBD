import React, { Component } from 'react';
import './FormContent.css';
import Form from "react-bootstrap/Form";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { loadItem, deleteItem } from '../actions/updateitem';
import { withRouter, Redirect } from 'react-router-dom';
import ConfModal from './ConfModal';
import store from '../store';
import { tokenConfig } from '../actions/auth';
import axios from 'axios';

class ModifyFormContent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            loading: true,
            blankError: "",
            successUpdate: ""
        }
    };

    static propTypes = {
        loadItem: PropTypes.func.isRequired,
    }

    componentDidMount() {
        const id = this.props.match.params.item_id;
        // console.log(id);
        this.props.loadItem(id);
        this.item(id);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.item_id !== this.props.match.params.item_id) {
            this.props.loadItem(this.props.match.params.item_id);
            this.item(this.props.match.params.item_id);
        }
    }

    item(id) {
        axios
            .get(`/api/products-user/${id}`, tokenConfig(store.getState))
            .then(response => { this.setState({ status: response.status, loading: false }) })
            .catch(err => { this.setState({ status: err.response.status, loading: false }) });
    }

    deleteProduct = (e) => {
        e.preventDefault();
        const id = this.props.match.params.item_id;
        axios
            .delete(`/api/products-user/${id}`, tokenConfig(store.getState))
            .then(response => { this.setState({ successUpdate: response.status }) })
            .catch(err => { this.setState({ blankError: err.response.status }) });

        this.setState({ isOpen: false });
    };

    toggleModal = (e) => {
        e.preventDefault();
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {

        const item_id = this.props.match.params.item_id;

        if (this.state.status === 404 || this.state.status === 500) {
            return <Redirect to="/404" />
        }

        if (this.state.loading) {
            return <div>Loading...</div>
        }

        return (
            <div>
                <div className="error_container">
                    <div className="sticky-container-item form_container">
                        <div className="error_container">
                            {this.state.blankError || this.state.status === 400 ? (<div className="alertMsg_container fade-in" ><div className="alert_msg">
                                <p className="msg_cont">Klaida! Prekė negali būti pašalinta</p>
                            </div></div>) : ''}

                            {this.state.successUpdate === 204 ? (<div className="alertMsg_container fade-in" ><div className="alertPsw_msg">
                                <p className="msg_cont">Prekė pašalinta sėkmingai</p>
                            </div></div>) : ''}
                        </div>
                    </div>
                </div>

                <div className="form_mainContainer">
                    <div className="form_container">
                        <h2 className="form_headline">Prekės informacija</h2>
                        <div className="form_box">
                            <Form className="form_content">

                                <div className="image_container">
                                    <img src={this.props.product.image} alt="" />
                                </div>

                                <div className="section_divider"></div>

                                <div className="dbox_container">
                                    <label className="label_title">
                                        Pavadinimas
                        </label>
                                    <p type="text" className="input_container" name="name">{this.props.product.name}</p>

                                    <label className="label_title">
                                        Aprašymas
                        </label>
                                    <p type="text" className="desc_box" name="description">{this.props.product.description}</p>
                                </div>

                                <div className="section_divider"></div>

                                <div className="categorybox_container">
                                    <label className="label_title">
                                        Kategorija
                        </label>
                                    <p type="text" className="input_container" name="category">{this.props.product.category}</p>

                                    <label className="label_title">
                                        Būklė
                        </label>
                                    <p type="text" className="input_container" name="state">{this.props.product.state}</p>


                                    <label className="label_title">
                                        Pradinės prekės vertė
                        </label>
                                    <p type="text" className="input_container" name="primary_value">{this.props.product.primary_value}</p>

                                    <div className="section_divider"></div>
                                    <div>
                                        <label className="label_title">
                                            Adresas
                            </label>
                                        <p type="text" className="input_container" name="address">{this.props.product.location}</p>
                                    </div>
                                </div>

                                <div className="section_divider"></div>

                                <div className="sell_container">
                                    <label className="label_title">
                                        Domina pardavimas
                    </label>
                                    <input type="checkbox" className="checkbox" checked={this.props.product.for_sale} />
                                </div>

                                {this.props.product.for_sale ? (<div>  <label className="label_title">
                                    Kaina
                        </label>
                                    <p type="text" className="input_container" name="name">{this.props.product.price}</p>
                                </div>) : ('')}


                                <div className="btn_position">
                                    <button className="cancelation_btn" onClick={this.toggleModal} >Ištrinti</button>
                                    {/* Modal for 'delete' action confirmation */}

                                    <ConfModal show={this.state.isOpen}>
                                        <p className="confirmation_message">Ar tikrai norite pašalinti skelbimą visam laikui?</p>
                                        <div className="psw_modiifybtn">
                                            <button className="cancel_Btn" onClick={this.toggleModal}>Atšaukti</button>
                                            <button className="valid_Btn" onClick={this.deleteProduct}>Patvirtinti</button>
                                        </div>
                                    </ConfModal>



                                    <Link to={"/prekes_redagavimas/" + item_id}>
                                        <button className="validation_btn">Redaguoti</button>
                                    </Link>
                                </div>

                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    product: state.updateitem.product
});

// load item, delete item

export default connect(mapStateToProps, { loadItem })(withRouter(ModifyFormContent))
