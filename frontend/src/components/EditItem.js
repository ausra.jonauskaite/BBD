import React, { Component } from 'react';
import './FormContent.css';
import Form from "react-bootstrap/Form";
import PropTypes from 'prop-types';
import { Link, withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";
// import { updateItem } from '../actions/updateitem';
import PrivateBicycle from './PublicRoutes/PrivateBicycle';
import store from '../store';
import { tokenConfig } from '../actions/auth';
import axios from 'axios';

class EditItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.product.id,
            name: this.props.product.name,
            description: this.props.product.description,
            selected: this.props.product.state,
            primary_value: this.props.product.primary_value,
            address: this.props.product.location,
            for_sale: this.props.product.for_sale,
            price: this.props.product.price,
            image: this.props.product.image,
            isChecked: this.props.product.for_sale,
            value: this.props.product.category,
            status: '',
            loading: true,
            blankError: false,
            updateSuccess: false
        };
        this.handleInput = this.handleInput.bind(this);
        this.handleCheckboxInput = this.handleCheckboxInput.bind(this);
        this.handleCategoryInput = this.handleCategoryInput.bind(this);
        this.handleStateInput = this.handleStateInput.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
    }

    state = {
        image: this.props.product.image,
        imgPreview: null,
        titleError: "",
        dboxError: "",
        primaryValueError: "",
        addressError: "",
        priceError: "",
    };

    componentDidMount() {
        const id = this.props.match.params.item_id;
        this.item(id);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.item_id !== this.props.match.params.item_id) {
            this.item(this.props.match.params.item_id);
        }
    }

    item(id) {
        axios
            .get(`/api/products-user/${id}`, tokenConfig(store.getState))
            .then(response => { this.setState({ status: response.status, loading: false }) })
            .catch(err => { this.setState({ status: err.response.status, loading: false }) });
    }

    itemUpdate(formData, id) {
        axios
            .patch(`/api/products-user/${id}`, formData, tokenConfig(store.getState))
            .then(response => { this.setState({ updateSuccess: response.status }) })
            .catch(err => { this.setState({ blankError: err.response.status }) });
    }

    handleInput(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleImageChange(event) {
        this.setState({
            image: event.target.files[0],
            imgPreview: URL.createObjectURL(event.target.files[0]),
        })
    };

    handleCheckboxInput() {
        this.setState({
            isChecked: !this.state.isChecked
        });
    }

    handleCategoryInput(event) {
        this.setState({
            value: event.target.value
        });
    }

    handleStateInput(event) {
        this.setState({
            selected: event.target.value
        });
    }

    // Title validation
    handleTitleChange = event => {
        this.setState({ name: event.target.value }, () => {
            this.validateTitle();
        });
    };

    validateTitle = () => {
        const { name } = this.state;

        if (name.length < 4 || name.length > 60 || name.length == 0) {
            this.setState({ titleError: 'Pavadinimą turi sudaryti bent 4 simboliai' });
        } else {
            this.setState({ titleError: '' });
        }
    };

    // Dbox validation
    handleDboxChange = event => {
        this.setState({ description: event.target.value }, () => {
            this.validateDbox();
        });
    };

    validateDbox = () => {
        const { description } = this.state;

        if (description.length == 0) {
            this.setState({ dboxError: 'Aprašymas negali būti tuščias' });
        } else {
            this.setState({ dboxError: '' });
        }
    };

    // Primary value validation
    handlePrimaryChange = event => {
        const primary_value = event.target.value;
        if (!primary_value || primary_value.match(/^\d{1,}(\.\d{0,2})?$/)) {
            this.setState({ primary_value }, () => {
                this.validatePrimary();
            });
        }

        // this.setState({ primary_value: value }, () => {
        //     this.validatePrimary();
        // });
    };

    validatePrimary = () => {
        const { primary_value } = this.state;

        if (primary_value.length == 0 || primary_value > 1000000) {
            this.setState({ primaryValueError: 'Neteisingai nurodyta pradinė prekė vertė' });
        } else {
            this.setState({ primaryValueError: '' });
        }
    };

    // Address validation
    handleAddressChange = event => {
        this.setState({ address: event.target.value }, () => {
            this.validateAddress();
        });
    };

    validateAddress = () => {
        const { address } = this.state;

        if (address.length < 6 || address.length > 50 || address.length == 0) {
            this.setState({ addressError: 'Neteisingas nurodytas adresas' });
        } else {
            this.setState({ addressError: '' });
        }
    };

    // Price validation
    handlePriceChange = event => {
        const price = event.target.value;
        if (!price || price.match(/^\d{1,}(\.\d{0,2})?$/)) {
            this.setState({ price }, () => {
                this.validatePrice();
            });
        }
        // this.setState({ price: event.target.value }, () => {
        //     this.validatePrice();
        // });
    };

    validatePrice = () => {
        const { price } = this.state;

        if (price.length == 0 || price > 1000000) {
            this.setState({ priceError: 'Neteisingai nurodyta prekės kaina' });
        } else {
            this.setState({ priceError: '' });
        }
    };

    finalValidation = () => {
        const { titleError, dboxError, primaryValueError, addressError, priceError } = this.state;
        if ((titleError === undefined || titleError === '') && (dboxError === undefined || dboxError === '') && (primaryValueError === undefined || primaryValueError === '') && (addressError === undefined || addressError === '') && (priceError === undefined || priceError === '')) {
            return true;
        } else {
            this.setState({ blankError: true });
            setTimeout(function () { this.setState({ blankError: false }) }.bind(this), 4000);
            return false;
        }

        // if (titleError != '' && dboxError != '' && primaryValueError != '' && addressError != '' && priceError != '') {
        //     this.setState({ blankError: true });
        //     setTimeout(function () { this.setState({ blankError: false }) }.bind(this), 4000);
        //     return false;
        // }
        // return true;
    }

    // SUBMIT FORM
    onSubmit = (event) => {

        event.preventDefault();

        // validation
        const isValid = this.finalValidation();

        if (isValid) {

            // const img = this.state.image;
            const imgPrev = this.state.imgPreview;

            if (!imgPrev) {
                const default_price = "0";
                const formData = new FormData();
                formData.append('id', this.state.id);
                formData.append('category', this.state.value);
                formData.append('name', this.state.name);
                formData.append('description', this.state.description);
                formData.append('state', this.state.selected);
                formData.append('primary_value', this.state.primary_value);
                formData.append('location', this.state.address);

                if (this.state.isChecked === true) {
                    formData.append('for_sale', this.state.isChecked);
                    formData.append('price', this.state.price);
                } else {
                    formData.append('for_sale', this.state.isChecked);
                    formData.append('price', default_price);
                }

                // console.log(Object.fromEntries(formData));
                // this.props.updateItem(formData, this.state.id);
                this.itemUpdate(formData, this.state.id);

            } else {
                const def_price = "0";
                const formData = new FormData();
                formData.append('id', this.state.id);
                formData.append('category', this.state.value);
                formData.append('name', this.state.name);
                formData.append('image', this.state.image);
                formData.append('description', this.state.description);
                formData.append('state', this.state.selected);
                formData.append('primary_value', this.state.primary_value);
                formData.append('location', this.state.address);

                if (this.state.isChecked === true) {
                    formData.append('for_sale', this.state.isChecked);
                    formData.append('price', this.state.price);
                } else {
                    formData.append('for_sale', this.state.isChecked);
                    formData.append('price', def_price);
                }

                // console.log(Object.fromEntries(formData));
                // this.props.updateItem(formData, this.state.id);
                this.itemUpdate(formData, this.state.id);
            }
        }

    };


    render() {

        if (this.state.status === 404 || this.state.status === 500) {
            return <Redirect to="/404" />
        }

        if (this.state.loading) {
            return <div>Loading...</div>
        }

        const { name, description, state, primary_value, for_sale, price, address, image, imgPreview } = this.state;

        return (
            <div>
                <div className="error_container">
                    <div className="sticky-container-item form_container">
                        <div className="error_container">
                            {this.state.blankError || this.state.status === 400 ? (<div className="alertMsg_container fade-in" ><div className="alert_msg">
                                <p className="msg_cont">Klaida! Neteisingai nurodyti duomenys</p>
                            </div></div>) : ''}

                            {this.state.updateSuccess ? (<div className="alertMsg_container fade-in" ><div className="alertPsw_msg">
                                <p className="msg_cont">Prekės informacija atnaujinta sėkmingai</p>
                            </div></div>) : ''}
                        </div>
                    </div>
                </div>

                <div className="form_mainContainer">
                    <div className="form_container">
                        <h2 className="form_headline">Redaguoti prekę</h2>
                        <div className="form_box">
                            <Form className="form_content" onSubmit={this.onSubmit.bind(this)}>

                                <div className="image_container">
                                    {this.state.imgPreview ?
                                        <img src={imgPreview} alt="" />
                                        : <img src={image} alt="" />}
                                </div>

                                <div className="image_upload">
                                    <input className="file" type="file" id="img_file" accept="image/png, image/jpeg, image/jpg" name="image" onChange={this.handleImageChange} />
                                    <label className="upload_btn1" htmlFor="img_file">Įkelti nuotrauką</label>
                                </div>

                                <div className="formats_msg">
                                    <p>Palaikomi nuotraukų formatai: JPEG, JPG, PNG</p>
                                    <p>Dydis: 1280x720</p>
                                </div>

                                <div className="section_divider_modify"></div>

                                <div className="dbox_container">
                                    <label className="label_title">
                                        Pavadinimas
                        </label>
                                    <input type="text" className="input_container" name="name" value={name} onChange={this.handleTitleChange} onBlur={this.validateTitle} />
                                    <div className='input_errors'>{this.state.titleError}</div>

                                    <label className="label_title">
                                        Aprašymas
                        </label>
                                    <textarea rows="150" className="desc_box" name="description" value={description} onChange={this.handleDboxChange} onBlur={this.validateDbox} />
                                    <div className='input_errors'>{this.state.dboxError}</div>
                                </div>

                                <div className="section_divider_modify"></div>

                                <div className="categorybox_container">
                                    <label className="label_title">
                                        Kategorija
                        </label>
                                    <select className="option_selection" onChange={this.handleCategoryInput} value={this.state.value}>
                                        <option value="Knygos">Knygos</option>
                                        <option value="Stalo žaidimai">Stalo žaidimai</option>
                                        <option value="Dviračiai / paspirtukai">Dviračiai / paspirtukai</option>
                                        <option value="Kitos prekės">Kitos prekės</option>
                                    </select>

                                    <label className="label_title">
                                        Būklė
                        </label>
                                    <select className="option_selection" onChange={this.handleStateInput} value={this.state.selected}>
                                        <option value="Nauja">Nauja</option>
                                        <option value="Labai gera">Labai gera</option>
                                        <option value="Gera">Gera</option>
                                        <option value="Patenkinama">Patenkinama</option>
                                    </select>

                                    <label className="label_title">
                                        Pradinė prekės vertė
                        </label>
                                    <input type="text" className="input_container" name="primary_value" value={primary_value} onChange={this.handlePrimaryChange} onBlur={this.validatePrimary} />
                                    <div className='input_errors'>{this.state.primaryValueError}</div>
                                    <div className="section_divider"></div>

                                    <div>
                                        <label className="label_title">
                                            Adresas
                            </label>
                                        <input type="text" className="input_container" name="address" value={address} onChange={this.handleAddressChange} onBlur={this.validateAddress} />
                                        <div className='input_errors'>{this.state.addressError}</div>
                                    </div>
                                </div>

                                <div className="section_divider_modify"></div>

                                <div className="sell_container">
                                    <label className="label_title">
                                        Domina pardavimas
                    </label>
                                    <input type="checkbox" className="checkbox" name="for_sale" value="for_sale" defaultChecked={this.state.isChecked} onChange={this.handleCheckboxInput} />
                                </div>

                                {this.state.isChecked ? (<div><label className="label_title">
                                    Kaina
                        </label>
                                    <input type="text" className="input_container" name="price" value={price} onChange={this.handlePriceChange} onBlur={this.validatePrice} /> <div className='input_errors'>{this.state.priceError}</div> </div>) : ('')}

                                <div className="btn_position">
                                    <Link to="/redaguoti_skelbimus"><button className="cancelation_btn">Atšaukti</button></Link>
                                    <button className="validation_btn" type="submit">Atnaujinti</button>
                                </div>

                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    product: state.updateitem.product
});

export default connect(mapStateToProps)(withRouter(EditItem))
