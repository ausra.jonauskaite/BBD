import React, { Component } from 'react';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import './HeartBtn.css';
import _ from 'lodash';
import ToggleBtn from './ToggleBtn';
import { postWishlist } from '../actions/wishlist';

class HeartBtn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            wishes: [
                { id: this.props.itemId, have: this.props.heart }
            ]
        };
    }


    toggleActive(wishId) {
        // console.log(wishId);
        let newState = Object.assign({}, this.state);
        let wish = _.find(newState.wishes, { id: wishId });
        // console.log(wish);
        wish.have = !wish.have;
        this.setState(newState);
        this.props.postWishlist(wishId);
    }

    render() {
        // console.log(this.state.wishes);
        // console.log(this.state.id);
        // console.log(this.state.have);
        return (<ToggleBtn wishes={this.state.wishes} toggleActive={(wishId) => this.toggleActive(wishId)} />);
    }
}

const mapStateToProps = state => ({
    item: state.privateitems.items
});

export default connect(mapStateToProps, { postWishlist })(HeartBtn)
