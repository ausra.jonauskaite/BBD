from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from django.contrib.auth import authenticate
from .models import Account, Message

# User Serializer


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('id', 'email', 'username',
                  'phone_number', 'address', 'image')

# Register Serializer


class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('id', 'email', 'username',
                  'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = Account.objects.create_user(
            validated_data['email'], validated_data['username'], validated_data['password'])

        return user

# Update Serializer


class UpdateSerializer(ModelSerializer):
    email = serializers.EmailField(required=True)

    class Meta:
        model = Account
        fields = ('email', 'username',
                  'phone_number', 'address', 'image')

    def validate_email(self, value):
        user = self.instance
        if Account.objects.exclude(pk=user.pk).filter(email=value).exists():
            raise serializers.ValidationError(
                {"email": "This email is already in use."})
        return value

    def save(self, *args, **kwargs):
        if self.validated_data.get('image', None) != None:
            if self.instance.image and self.instance.image != 'default/default.png':
                self.instance.image.delete()
        return super().save(*args, **kwargs)


# Change Password Serializer


class ChangePasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True)
    password2 = serializers.CharField(write_only=True, required=True)
    old_password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = Account
        fields = ('old_password', 'password', 'password2')

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError(
                {"password": "Password fields didn't match."})

        return attrs

    def validate_old_password(self, value):
        user = self.instance
        if not user.check_password(value):
            raise serializers.ValidationError(
                {"old_password": "Old password is not correct"})
        return value

    def update(self, instance, validated_data):

        instance.set_password(validated_data['password'])
        instance.save()

        return instance

# Update profile photo Serializer


class ProfilePhotoSerializer(ModelSerializer):
    class Meta:
        model = Account
        fields = ["image"]

    def save(self, *args, **kwargs):
        if self.instance.image and self.instance.image != 'default/default.png':
            self.instance.image.delete()
        return super().save(*args, **kwargs)

# Login Serializer


class LoginSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        user = authenticate(**data)
        if user and user.is_active:
            return user
        raise serializers.ValidationError("Incorrect Credentials")

# Email Serializer


class EmailSerializer(serializers.Serializer):
    get_item = serializers.CharField(required=False)
    give_item = serializers.CharField(required=False)
    message = serializers.CharField(required=False)
    show_email = serializers.BooleanField()
    show_phone = serializers.BooleanField()

    class Meta:
        model = Message
        fields = ['message', 'give_item', 'get_item',
                  'show_email', 'show_phone']


# Message Serializer


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('username', 'image')


class MessageViewSerializer(serializers.ModelSerializer):
    sender = ProfileSerializer(many=False, read_only=True)

    class Meta:
        model = Message
        fields = ['id', 'sender', 'message', 'is_read', 'timestamp']
