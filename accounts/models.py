import os
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _


def upload_to(instance, filename):
    base, extension = os.path.splitext(filename.lower())
    return f"profile_photo/{instance.username}{'_ID_'}{instance.pk}{extension}"
    # return 'profile_photo/{filename}'.format(filename=filename)


class MyAccountManager(BaseUserManager):
    def create_user(self, email, username, password=None):
        if not email:
            raise ValueError('Users must have an email address')
        if not username:
            raise ValueError('Users must have a username')

        user = self.model(
            email=self.normalize_email(email),
            username=username,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(
            email=self.normalize_email(email),
            password=password,
            username=username,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Account(AbstractBaseUser):
    email = models.EmailField(verbose_name="email", max_length=60, unique=True)
    username = models.CharField(max_length=30, unique=True)
    date_joined = models.DateTimeField(
        verbose_name='date joined', auto_now_add=True)
    last_login = models.DateTimeField(verbose_name='last login', auto_now=True)
    phone_number = models.CharField(blank=True, max_length=15)
    address = models.CharField(blank=True, max_length=50)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    image = models.ImageField(
        _("image"), upload_to=upload_to, default='default/default.png')

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    objects = MyAccountManager()

    def delete(self):
        self.image.storage.delete(self.image.name)
        super().delete()

    def image_tag(self):
        return mark_safe('<img src="/../../media/%s" width="30" height="30" />' % (self.image))

    image_tag.short_description = 'Image'

    def __str__(self):
        return self.username

    # For checking permissions
    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True


class Message(models.Model):
    id = models.AutoField(primary_key=True)
    sender = models.ForeignKey(
        Account, on_delete=models.CASCADE, related_name='sender')
    receiver = models.ForeignKey(
        Account, on_delete=models.CASCADE, related_name='receiver')
    message = models.TextField(max_length=1200)
    timestamp = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)

    def __str__(self):
        return self.message

    class Meta:
        ordering = ('-timestamp',)
