from rest_framework import generics, permissions
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import get_object_or_404
from knox.models import AuthToken
from .serializers import UserSerializer, RegisterSerializer, LoginSerializer, UpdateSerializer, ChangePasswordSerializer, ProfilePhotoSerializer, EmailSerializer, MessageViewSerializer
from .models import Account, Message
from product.models import Product
from django.core.mail import EmailMessage, send_mail

# Register API


class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })

# Login API


class LoginAPI(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data
        _, token = AuthToken.objects.create(user)
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": token
        })

# Update Profile Photo API


class ProfilePhotoUploadAPI(APIView):
    parser_classes = [MultiPartParser, FormParser]
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def post(self, request, format=None):
        serializer = ProfilePhotoSerializer(
            data=request.data, instance=request.user)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# Update User API


class UpdateAPI(APIView):
    parser_classes = [MultiPartParser, FormParser]
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def patch(self, request, format=None):
        serializer = UpdateSerializer(
            data=request.data, instance=request.user, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# Change User Password API


class ChangePasswordAPI(APIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def post(self, request, format=None):
        serializer = ChangePasswordSerializer(
            data=request.data, instance=request.user)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# Get User API


class UserAPI(generics.RetrieveAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user

# Send email API


def send_email(email_data):
    email = EmailMessage(subject=email_data['email_subject'],
                         body=email_data['email_body'], to=[email_data['to_email'], ])
    email.send()


class SendMailAPI(generics.GenericAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = EmailSerializer

    def post(self, request):
        serializer = self.get_serializer(
            data=request.data)

        if serializer.is_valid():
            sender_obj = request.user
            get_product = Product.objects.get(id=serializer.data['get_item'])
            receiver_obj = get_product.creator
            info = '\n\nVartotojo ' + sender_obj.username + ' kontaktinė informacija:'
            if serializer.data['show_email'] == True:
                info = info + '\nEl. pašto adresas: ' + sender_obj.email
            if serializer.data['show_phone'] == True:
                info = info + '\nTelefono numeris: ' + sender_obj.phone_number

            if serializer.data.get('give_item', None) != None:
                give_product = Product.objects.get(
                    id=serializer.data['give_item'])
                offer = give_product.name
            else:
                offer = 'Nenurodyta'

            email_text = 'Skelbimas, kuris domina: ' + get_product.name + '\n\nPrekė į kurią norima mainyti: ' \
                + offer + info + '\n\nKomentaras:\n' + \
                serializer.data['message'] + \
                '\n\nAutomatinis Barter_app pranešimas ;)'

            message_text = 'Sudominęs skelbimas: ' + get_product.name + '\n\nPasiūlymas: ' + offer + \
                '\n\nKomentaras:\n' + serializer.data['message'] + info

            email_data = {'to_email': receiver_obj.email,
                          'email_subject': 'Vartotojas ' + sender_obj.username + ' domisi jūsų skelbimu!', 'email_body': email_text}
            send_email(email_data)

            Message.objects.create(
                sender=sender_obj, receiver=receiver_obj, message=message_text)

            return Response({'detail': 'Email sent to user'}, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

# View notifications api


class NotificationstAPI(generics.ListAPIView):
    serializer_class = MessageViewSerializer
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def get_queryset(self):
        user = self.request.user
        queryset = Message.objects.filter(receiver=user.pk)
        return queryset


# Set notification to is read API

class MarkAsReadAPI(APIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def post(self, request):
        message = get_object_or_404(Message, id=request.data.get('id'))
        user = self.request.user
        if message.receiver == user:
            if message.is_read == False:
                # setattr(message, 'is_read', True)
                message.is_read = True
                message.save()
                return Response({'detail': 'Message marked as read'}, status=status.HTTP_200_OK)
            else:
                return Response({'detail': 'Message is already read'}, status=status.HTTP_200_OK)
        return Response({'detail': 'An error has occurred'}, status=status.HTTP_400_BAD_REQUEST)


# Set all users notification to is read API

class MarkAllAsReadAPI(APIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def post(self, request):
        user = self.request.user
        if Message.objects.filter(receiver=user.pk, is_read=False):
            Message.objects.filter(
                receiver=user.pk, is_read=False).update(is_read=True)
            return Response({'detail': 'All user messages marked as read'}, status=status.HTTP_200_OK)
        else:
            return Response({'detail': 'All messages is already read'}, status=status.HTTP_200_OK)
        return Response({'detail': 'An error has occurred'}, status=status.HTTP_400_BAD_REQUEST)
