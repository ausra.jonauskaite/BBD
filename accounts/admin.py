from django.contrib import admin
from django.contrib import messages
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
from .models import Account, Message
from django.utils.html import format_html

admin.site.unregister(Group)


class AccountAdmin(UserAdmin):
    list_display = ('email', 'username', 'phone_number', 'address',
                    'date_joined', 'last_login', 'is_admin', 'id', 'image_tag')
    search_fields = ('email', 'username',)
    readonly_fields = ('image_preview', 'id', 'date_joined', 'last_login')

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

    def image_preview(self, obj):
        return format_html('<img src="{}" width="300" height="300" />'.format(obj.image.url))

    image_preview.short_description = 'Image preview'
    image_preview.allow_tags = True


admin.site.register(Account, AccountAdmin)


class MessageAdmin(admin.ModelAdmin):
    list_display = ('receiver', 'sender', 'message',
                    'timestamp', 'is_read', 'id')
    search_fields = ('sender', 'receiver',)
    readonly_fields = ('id', 'timestamp')

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

    def is_read_true(modeladmin, request, queryset):
        queryset.update(is_read=True)
        messages.success(request, "Selected Record(s) Marked as IS read !")

    def is_read_false(modeladmin, request, queryset):
        queryset.update(is_read=False)
        messages.success(request, "Selected Record(s) Marked as IS NOT read !")

    admin.site.add_action(is_read_true, "Set as read")
    admin.site.add_action(is_read_false, "Set as not read")


admin.site.register(Message, MessageAdmin)
