from django.urls import path, include
from .api import RegisterAPI, LoginAPI, UserAPI, UpdateAPI, ChangePasswordAPI, ProfilePhotoUploadAPI, SendMailAPI, NotificationstAPI, MarkAsReadAPI, MarkAllAsReadAPI
from knox import views as knox_views

urlpatterns = [
    path('api/auth', include('knox.urls')),
    path('api/auth/register', RegisterAPI.as_view(), name='registration'),
    path('api/auth/login', LoginAPI.as_view(), name='logout'),
    path('api/auth/user', UserAPI.as_view(), name='get user info'),
    path('api/auth/update', UpdateAPI.as_view(), name='update user profile'),
    path('api/auth/change', ChangePasswordAPI.as_view(), name='change password'),
    path("api/auth/profile-photo", ProfilePhotoUploadAPI.as_view(),
         name='update only profile photo'),
    path('api/auth/send', SendMailAPI.as_view(), name='send email'),
    path('api/auth/messages', NotificationstAPI.as_view(), name='view messages'),
    path('api/auth/markasread', MarkAsReadAPI.as_view(), name='mark as read'),
    path('api/auth/markallasread',
         MarkAllAsReadAPI.as_view(), name='mark all as read'),
    path('api/auth/logout', knox_views.LogoutView.as_view(), name='knox_logout')
]
