from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from rest_framework.schemas import get_schema_view
from rest_framework.documentation import include_docs_urls
from rest_framework import routers
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView


urlpatterns = [
    path('', include('frontend.urls')),
    path('', include('product.urls')),
    path('', include('accounts.urls')),
    path('', include('image_classification.urls')),
    path('admin/', admin.site.urls),
    path('coreapi/', include_docs_urls(title='AppAPI')),
    path('openapi/', get_schema_view(
        title="AppAPI",
        description="API for the App",
        version="1.0.0"
    ), name='openapi-schema'),
    path('docs/', TemplateView.as_view(
        template_name='documentation.html',
        extra_context={'schema_url': 'openapi-schema'}
    ), name='swagger-ui'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = "Barter app admin site"
admin.site.site_title = "Barter app admin site"
admin.site.index_title = "Barter App Admin"
