from django.urls import path
from .api import ClassificationAPI


urlpatterns = [
    path('api/class', ClassificationAPI.as_view()),
]
