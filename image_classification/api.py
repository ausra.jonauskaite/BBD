import os
from keras.preprocessing import image
from keras.models import load_model
import numpy as np
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions, status
from django.core.files.storage import default_storage

import numpy as np
from django.conf import settings
from django.core.files.storage import default_storage
from django.shortcuts import render
from keras.models import load_model
from keras.preprocessing import image


class ClassificationAPI(APIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def post(self, request, format=None):
        user = self.request.user
        file = request.FILES["imageFile"]
        extension = os.path.splitext(file.name.lower())
        name = f"temp_class/{'photo_user_ID_'}{user.pk}{extension[1]}"
        file_name = default_storage.save(name, file)
        file_url = default_storage.path(file_name)

        # dimensions of our images
        img_width, img_height = 224, 224

        # load the model we saved
        model = load_model('system_model/class_model.h5')
        model.compile(loss='categorical_crossentropy',
                      optimizer='adam',
                      metrics=['accuracy'])

        # predicting images
        file = image.load_img(file_url, target_size=(img_width, img_height))
        x = image.img_to_array(file)
        x = np.expand_dims(x, axis=0)

        images = np.vstack([x])
        classes = model.predict_classes(images, batch_size=10)
        label = ['Dviračiai / paspirtukai',
                 'Kitos prekės', 'Knygos', 'Stalo žaidimai']
        if classes == [0]:
            prediction = label[0]
        elif classes == [1]:
            prediction = label[1]
        elif classes == [2]:
            prediction = label[2]
        else:
            prediction = label[3]

        default_storage.delete(file_name)

        return Response({'category': prediction}, status=status.HTTP_200_OK)
